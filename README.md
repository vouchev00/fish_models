This repository is home for trainable fish models and give them a surrounding framework.

- Load robofish.io files as dataset (including raycasting and actions)
- Clean interface to guppy gym as simulator
- Storing and loading models


## <a href="http://agerken.de/fish_models/index.html">Documentation</a>


## Installation

Clone repository
```bash
git clone https://git.imp.fu-berlin.de/bioroboticslab/robofish/fish_models.git
```

Create and activate a virtual environment. (tested with python 3.8.0)
```bash
cd fish_models
python3 -m venv venv
source venv/bin/activate
```

Install this package
```bash
python3 -m pip install -e . --extra-index-url https://git.imp.fu-berlin.de/api/v4/projects/6392/packages/pypi/simple
```

# FAQ

## Why is the content of the *data/* directory ignored?
Normally data should not be uploaded to a git repository. By providing the ignored *data/* directory, you can still have data used for your experiments in the same place as the experiment code, without pushing it to git.
