This repository is home for trainable fish models and gives them a surrounding framework.

- Load robofish.io files as a dataset (including raycasting and actions)
- Clean interface to guppy gym as the simulator
- Storing and loading models

<a href="https://git.imp.fu-berlin.de/bioroboticslab/robofish/fish_models" class="myButton">Code</a>


# Installation

Clone repository
```bash
git clone https://git.imp.fu-berlin.de/bioroboticslab/robofish/fish_models.git
cd fish_models
```

Install this package
```bash
python3 -m pip install -e . --extra-index-url https://git.imp.fu-berlin.de/api/v4/projects/6392/packages/pypi/simple
```

# Usage
The repository consists of multiple parts, which will be explained shortly here:

<img src="img/gym_interface_structure.png" alt="Gym interface structure" width="400"/>

The motivation of the framework is to train models and do inference with them, using `raycasts` as observation space and `turn + speed` as action space. During training, this information has to be extracted from datasets, during inference we want to use [robofish.gym_guppy](https://git.imp.fu-berlin.de/bioroboticslab/robofish/gym-guppy) as a simulator.

There are two training examples in this Documentation [Task 1 - Simple Model](#task-1-simple-model) & [Task 2 - Using Datasets](#task-2-using-datasets).

## Raycast

<img src="img/raycast.png" alt="Raycast" width="400"/>

The observations of the virtual fishes consist of raycasts of the walls (left) and view bins of other fishes (right).

```python
import fish_models

raycast = fish_models.gym_interface.Raycast(
            n_fish_bins=4,
            n_wall_raycasts=5,
            fov_angle_fish_bins=np.pi,
            fov_angle_wall_raycasts=np.pi,
            world_bounds=([-50, -50], [50, 50]),
        )
raycast.cast(
  state=np.array([[0, 0, 0], [10, 10, 0]]),
  focal_fish_id=0
)
```
The code above generates the raycast options as in the image and gets raycasts for two fishes with poses (x,y, orientation) [0,0,0] and [10,10,0] in a fish tank with the size 100 x 100 cm.
```
>> array([0., 0., 0., 0.9, 0.65, 0.5, 0.65, 0.5, 0.65], dtype=float32)
```

<img src="img/raycast_example.png" alt="Raycast" width="400"/>


The output gives back values between 0 and 1, where 0 means far away and 1 means close. 1 is set to be the diagonal distance through the tank (~142cm in this case).

The return consists of `n_fish_bin` values for fish (here 4) and `n_wall_raycasts` for the walls (here 5). In the output, we can read that there is a close fish in the most left view bin and that the wall distances change from 0.65 (wall centers) to 0.5 (corners).

⚠️ The order of raycasts is counterclockwise, coming from the definition of radiants. This means that the first value in the array is the most right bin from the perspective of the fish.


## Gym Interface
To interface with `robofish.gym_guppy`, only a class that inherits from `AbstractRaycastBasedModel` has to be implemented. The only function, that has to be implemented is `choose_action`. It gets a [raycast observation](#raycast) and should return speed [cm/s] and turn [rad/s].

```python
import fish_models
import numpy as np

class RandomModel(fish_models.gym_interface.AbstractRaycastBasedModel):
    def choose_action(self, view: np.ndarray):
        # Return speed and turn from view
        speed = np.random.random() * 5.
        turn = (np.random.random() - 0.5) * 5.
        return speed, turn
```

Normally, this class will have training methods and implement a fancy model. If real data should be used for training, checkout [IO Dataset](#io-dataset).

To generate a new track using a model, we need a model, a raycast object, the world size, and the sampling frequency (in our data normally 25hz).

```python
import fish_models
import numpy as np

# Lets use the random model from above
model = RandomModel()
raycast = fish_models.gym_interface.Raycast(
            n_wall_raycasts=5,
            n_fish_bins=4,
            fov_angle_fish_bins=np.pi,
            fov_angle_wall_raycasts=np.pi,
            world_bounds=([-50, -50], [50, 50]),
        )

generator = fish_models.gym_interface.TrackGeneratorGymRaycast(
    model, raycast, [100,100], 25
)

track = generator.create_track(n_guppies=2, trackset_len=1000)
print(track.shape)
```

`>> (2, 1000, 3)`

We created a new track, with 2 fish, 1000 timesteps, and poses (x,y, orientation). Let's save it as io file and plot it!

```python
f = generator.as_io_file(track)
f.save_as("__generated.hdf5")
```

```python
import matplotlib.pyplot as plt

plt.figure(figsize=(10,10))
plt.xlim(-50,50)
plt.ylim(-50,50)
for fish_id in range(2):
    plt.plot(track[fish_id, :, 0], track[fish_id, :, 1])
plt.show()
```
<img src="img/random_model.png" alt="Random Model" width="300"/>



## Task 1 - Simple model
~~~text
📝 Try out the code on your computer!
Can you create a fish, that always swims straight,
and turns left, when the wall is too close?
~~~


## IO Dataset
To train models, that behave like real fish, we want to access real data easily. This is covered by IO Datasets.
Public data of two female guppies is available in this [live_female_female.zip](http://agerken.de/upload/live_female_female.zip). More Data is available in the track database (access required).

```python
# Copy raycast from above and set the file folder.
from pathlib import Path
import fish_models

data_folder = Path("/path/to/files")
raycast = # copy from above

dset = fish_models.datasets.io_dataset.IoDataset(
    data_folder,
    raycast,
    output_strings=["poses", "actions", "views"],
    reduce_dim=2,
    max_files=10,
)
```

After specifying the data folder and raycast object, we state that we require `poses, actions, views` from a maximum of 10 files.

```
Loading data from 10 files.
100%|██████████████████████████| 10/10 [00:01<00:00,  9.03it/s]
Calculating views from 10 files.
100%|██████████████████████████| 10/10 [00:10<00:00,  1.01s/it]
Created IoDataset:
Reduced the first 3 dimensions from (10, 2, 8989) to (179780)
poses	(179780, 3):	consisting of x, y, calc_ori_rad.
actions	(179760, 2):	consisting of speed[cm/s] and turn [rad/s].
views	(179760, 9):	4 fish_bins and 5 wall ray casts.
```

The output tells us, that from 10 files and two fish each, there were 8989 timesteps per file. If this separation is important for your model, you can keep it. If not, dimensions can be reduced with `reduce_dim`. In our case, we reduced two dimensions and reduced them from (10, 2, 8989) to (179780).

### Accessing the data
```python
# all actions
dset["actions"].shape
>> (179760, 2)

# All information about the first row
dset[0]
>> {'poses': array([11.2 , 37.4 ,  2.68]),
 'actions': array([6.58, 0.62]),
 'views': array([0.  , 0.  , 0.  , 0.  , 0.9 , 0.91, 0.8 , 0.54, 0.31])}

# All information in a dict
dset[:]
>> {'poses': array([[ 11.2 ,  37.4 ,   2.68],
        [ 10.96,  37.51,   2.7 ],
        ...,
        [-47.12, -21.01,   1.86],
        [-47.18, -20.85,   1.97]]),
 'actions': array([[6.58, 0.62],
        [6.17, 1.64],
        ...,
        [3.34, 1.24],
        [4.15, 2.66]]),
 'views': array([[0.  , 0.  , 0.  , ..., 0.8 , 0.54, 0.31],
        [0.  , 0.  , 0.  , ..., 0.79, 0.54, 0.32],
        ...,
        [0.  , 0.  , 0.  , ..., 0.91, 0.98, 0.98],
        [0.  , 0.  , 0.  , ..., 0.93, 0.98, 0.98]])}
```

## Task 2 - Using Datasets
~~~text
📝 Try out the code on your computer!
Can you create a histogram of speeds?
~~~

## Upcoming Features
Features coming up:

- Saving and restoring models
- Saving and restoring datasets

## Further questions
If there are further questions, please text Andi in Mattermost or by mail [andi.gerken@gmail.com].

<style>
.myButton {
	background:#3971cc;
	background-color:#3971cc;
	border-radius:9px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:20px;
	padding:8px 16px;
	text-decoration:none;
}
.myButton:hover {
	background:#396bd1;
	background-color:#396bd1;
  color: #e0e0e0;
  text-decoration: none;
}
.myButton:active {
	position:relative;
	top:1px;
}
</style>
