

```
bins = 1
raycast_options = {
    "n_fish_bins": bins,
    "n_wall_raycasts": bins,
    "fov_angle_fish_bins": 2 * np.pi,
    "fov_angle_wall_raycasts": 2 * np.pi,
    "world_bounds": ([-50, -50], [50, 50]),
    # "far_plane": 142,
}

# Raycast in cm
raycast = fish_models.gym_interface.Raycast(**raycast_options)

# IO Files in cm, actions in m/s
data_folder = Path("/home/andi/mnt/trackdb_offline/trainingdata/live_female_female/train")
dset = fish_models.datasets.io_dataset.IoDataset(data_folder, raycast, output_strings=["poses","action","view"],max_files=1)
```

```
#
dset[:]
dset[:]