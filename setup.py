# SPDX-License-Identifier: LGPL-3.0-or-later

from subprocess import run, PIPE
from setuptools import setup, find_packages
import site
import sys

site.ENABLE_USER_SITE = "--user" in sys.argv[1:]


def source_version():
    version_parts = (
        run(
            ["git", "describe", "--tags", "--dirty"],
            check=True,
            stdout=PIPE,
            encoding="utf-8",
        )
        .stdout.strip()
        .split("-")
    )

    if version_parts[-1] == "dirty":
        dirty = True
        version_parts = version_parts[:-1]
    else:
        dirty = False

    version = version_parts[0]
    if len(version_parts) == 3:
        version += ".post0"
        version += f".dev{version_parts[1]}+{version_parts[2]}"
    if dirty:
        version += "+dirty"

    return version


# run(
#     [
#         "python3",
#         "-m",
#         "pip",
#         "install",
#         "-e ",
#         "git+https://git.imp.fu-berlin.de/bioroboticslab/robofish/gym-guppy.git@master#egg=robofish-gym-guppy",
#     ],
#     check=True,
#     stdout=PIPE,
#     encoding="utf-8",
# )  # Install gym_guppy from sources. This is a workaround until gym guppy has artifacts

setup(
    name="fish_models",
    version=source_version(),
    author="",
    author_email="",
    dependency_links=[
        "https://git.imp.fu-berlin.de/api/v4/projects/6392/packages/pypi/simple",
        "https://pypi.python.org/simple/",
    ],
    install_requires=[
        "robofish-gym-guppy",
        # "numpy~=1.19.2",
        "tensorflow",
        "robofish-io",
        "robofish-core",
        "deprecation",
        "pytest",
        "torch",
        "captum",
        "matplotlib",
        "pandas",
        "scikit-image",
        "scikit-learn",
        "tqdm",
    ],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    python_requires=">=3.7",
    packages=[f"fish_models.{p}" for p in find_packages("src/fish_models")],
    package_dir={"": "src"},
    zip_safe=True,
)
