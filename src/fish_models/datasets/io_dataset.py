""" .. include:: ../../../docs/io_dataset.md """

from pathlib import Path
from typing import Iterable, List, Union

import numpy as np
import robofish.io
import torch
from robofish.io.entity import Entity
from tqdm import tqdm

# TODO: Find better place for Raycast
import fish_models


class IoDataset(torch.utils.data.Dataset):
    """
    Configurable dataset created from robofish IO .hdf5 file(s)

    Look at the Args descriptions of output_strings and reduce_dim for options
    on how to configure this dataset

    Args:
        data_dir: Path
            Path to the directory with the .hdf5 files
        raycast: gym_interface.Raycast
            For use with the "views" output string
        glob: str, default: "*.hdf5"
            Regex for finding only specifically named .hdf5 files.
        max_files: int, optional
            Upper limit on umber of files read. Default: None
        speed_bin_borders: List[float], optional
            For use with the "actions_binned" output string
        turn_angle_bin_borders: List[float], optional
            For use with the "actions_binned" output string
        output_strings: List[str], default: ["views", "actions"]
            List that defines the list returned by the __getitem__ method
            The strings in the list can correspond to names of attributes
            of this dataset (see below).
        reduce_dim: Union[int,str], default: 0
            Collapse shape of list items returned by the __getitem__ method
            For natural shape, see attributes description below
            Options:
                None / "none" / 0 - Natural shape is preserved
                "file+fish" / 1 - Collapse first two dimensions
                "file+fish+timestep" / 2 - Collapse first three dimensions

    Attributes:
        self.poses :
            shape = (n_files, n_fishes, n_timesteps, 3)
            the 3 values in the last dimension are
                0: x-offset from center in cm
                1: y-offset from center in cm
                2: orientation in rad (0 means the fish is facing "right"
        self.views :
            NOTE: Requires set raycast
            WARNING: Check your raycast object to see in what way this
            can be interpreted as a FOV
            shape = (n_files, n_fishes, n_timesteps, n_wall_rays+n_fish_rays)
            the n_wall_raycasts + n_fish_raycasts values in the last dim. are
                0 to n_wall_raycasts-1: Closeness of walls
                n_wall_raycasts to n_fish_raycasts-1: Closeness of other fish
        self.actions :
            shape = (n_files, n_fishes, n_timesteps, 2)
            the 2 values in the last dimension are
                0: speed in cm/s
                1: turn speed in rad/s
        self.actions_binned :
            NOTE: Requires set speed_bin_borders and/or turn_angle_bin_borders
            if only one is set, shape = (n_files, n_fishes, n_timesteps)
            if both are set, shape = (n_files, n_fishes, n_timesteps, 2)
    """

    def __init__(
        self,
        data_dir: Path,
        raycast: "gym_interface.Raycast",
        *,
        glob: str = "*.hdf5",
        max_files: int = None,
        speed_bin_borders: List[float] = None,
        turn_angle_bin_borders: List[float] = None,
        output_strings: List[Union[int, str]] = ["views", "actions"],
        reduce_dim: Union[int, str] = 0,
    ):
        assert data_dir.exists() and data_dir.is_dir()
        files = list(data_dir.glob(glob))
        files = files if max_files is None else files[:max_files]

        self.files = files
        self.raycast = raycast
        self.output_strings = output_strings
        self.reduce_dim = reduce_dim
        self.actions_bin_borders = (speed_bin_borders, turn_angle_bin_borders)
        self.attrs = {}
        self.initialized = False

        possible_output_strings = ["views", "actions", "poses", "actions_binned"]
        assert all(
            [o in possible_output_strings for o in output_strings]
        ), f"The requested output strings were not valid. Valid options: {possible_output_strings}, Requested: {output_strings}"

        print(f"Loading data from {len(files)} files.")

        for file_id, path in enumerate(tqdm(files)):

            with robofish.io.File(path=path) as f:
                # Save attrs, check that they are equal
                for p in ["world_size", "frequency"]:
                    assert (
                        p not in self.attrs or (self.attrs[p] == getattr(f, p)).all()
                    ), f"The {p} of a file was not the same as in the group of files ({self.attrs[p]})"
                    self.attrs[p] = getattr(f, p)

                # Speed in cm/step, turn in rad/step
                speed_turn = f.entity_speeds_turns
                # Speed in cm/s, turn in rad/s (n_fish, n_timesteps, n_actions)
                speed_turn *= f.frequency

                # Poses (n_fish, timesteps, 3)
                poses = f.entity_poses_calc_ori_rad

                if not self.initialized:
                    self.initialized = True
                    files_shape = (len(files),)
                    self.actions = np.empty(files_shape + speed_turn.shape)
                    self.poses = np.empty(files_shape + poses.shape)

                    # Views will only be calculated by set mode, when they appear in the output strings.
                    self.views = None
                    # Same for binned actions
                    self.actions_binned = None

                self.actions[file_id] = speed_turn
                self.poses[file_id] = poses

        self.set_mode(output_strings, reduce_dim)

        msg = f"Created IoDataset:\n"
        if reduce_dim > 0:
            msg += f"Reduced the first {reduce_dim + 1} dimensions from {self.poses.shape[:reduce_dim+1]} to ({self.prepared_output[output_strings[0]].shape[0]})\n"
        dims = ["file", "fish", "timesteps"]

        # msg += f"Shape: self.shape"
        for output_string in output_strings:
            msg += f"{output_string}\t{self.prepared_output[output_string].shape}:\t"
            if output_string == "views":
                msg += f"{self.raycast.n_fish_bins} fish_bins and {self.raycast.n_wall_raycasts} wall ray casts."
            elif output_string == "actions":
                msg += f"consisting of speed[cm/s] and turn [rad/s]."
            elif output_string == "poses":
                msg += f"consisting of x, y, calc_ori_rad."
            msg += "\n"

        print(msg)

    def set_mode(self, output_strings, reduce_dim=0):
        named_reduce_dim = {None: 0, "none": 0, "file+fish": 1, "file+fish+timestep": 2}
        if reduce_dim in named_reduce_dim:
            reduce_dim = named_reduce_dim[reduce_dim]

        if "views" in output_strings and self.views is None:
            file_shape = (len(self.files),)

            print(f"Calculating views from {len(self.files)} files.")
            for file_id in tqdm(range(self.poses.shape[0])):
                # views (n_fish, timesteps, fish_bins + view_bins)
                # There is no action for the last timestep so we don't need the last view
                views = self.raycast.cast_array(self.poses[file_id])[:, :-1]
                if self.views is None:
                    self.views = np.empty(file_shape + views.shape)
                self.views[file_id] = views

        if "actions_binned" in output_strings and self.actions_binned is None:
            assert self.actions is not None

            speed_bin_borders, turn_angle_bin_borders = self.actions_bin_borders

            def bin_speeds():
                assert len(speed_bin_borders) >= 3
                return (
                    np.digitize(self.actions[:, :, :, 0], speed_bin_borders).clip(
                        1, len(speed_bin_borders) - 1
                    )
                    - 1
                )

            def bin_turns():
                assert len(turn_angle_bin_borders) >= 3
                return (
                    np.digitize(self.actions[:, :, :, 1], turn_angle_bin_borders).clip(
                        1, len(turn_angle_bin_borders) - 1
                    )
                    - 1
                )

            if speed_bin_borders is not None and turn_angle_bin_borders is None:
                self.actions_binned = bin_speeds()
            elif speed_bin_borders is None and turn_angle_bin_borders is not None:
                self.actions_binned = bin_turns()
            else:
                assert (
                    speed_bin_borders is not None and turn_angle_bin_borders is not None
                )
                self.actions_binned = np.stack([bin_speeds(), bin_turns()], axis=-1)

        reduced_shape = (
            np.prod(self.poses.shape[: reduce_dim + 1]),
        ) + self.poses.shape[reduce_dim + 1 :]

        self.prepared_output = {}
        self.output_shapes = {}
        for output_string in output_strings:
            arr = getattr(self, output_string)
            reduced_shape = (np.prod(arr.shape[: reduce_dim + 1]),) + arr.shape[
                reduce_dim + 1 :
            ]
            reshaped_arr = arr.reshape(reduced_shape)
            self.output_shapes.update({output_string: reduced_shape})
            self.prepared_output.update({output_string: reshaped_arr})

    def __len__(self):
        first_key = self.output_strings[0]
        return self.prepared_output[first_key].shape[0]

    def __getitem__(self, index):
        if index in self.output_strings:
            return self.prepared_output[index]
        else:
            return {k: v[index] for k, v in self.prepared_output.items()}

    @property
    def shape(self):
        return self.output_shapes
        # keys = list(self.output_shapes.keys())
        # return (self.output_shapes[keys[0] : -1],) + (
        #    {k: self.output_shapes[k] for k, v in keys.items()},
        # )

    @property
    def world_size(self):
        return self.attrs["world_size"]

    @property
    def frequency(self):
        return self.attrs["frequency"]


if __name__ == "__main__":
    raycast_options = {
        "n_fish_bins": 5,
        "n_wall_raycasts": 10,
        "fov_angle_fish_bins": np.pi,
        "fov_angle_wall_raycasts": np.pi,
        "world_bounds": ([-50, -50], [50, 50]),
    }

    data_dir = Path(
        "/home/andi/mnt/trackdb_offline/trainingdata/live_female_female/test/"
    )

    raycast = fish_models.gym_interface.Raycast(**raycast_options)
    dset = RaycastDataset(data_dir, raycast, max_files=2)
    print(dset[0])

    # import matplotlib.pyplot as plt

    # speeds = [dset[i][1][0] for i in range(len(dset))]
    # plt.hist(speeds, bins=100)
    # plt.show()

    # TODO
