import abc
import functools
from typing import Callable, Tuple

try:
    import cPickle as pickle
except:
    import pickle

import inspect

import deprecation
import numpy as np
import robofish.core
import robofish.gym_guppy.envs
import robofish.gym_guppy.guppies
import robofish.io
import robofish.io.utils as utils
from tqdm import tqdm

np.set_printoptions(suppress=True, precision=2)


import numpy as np

import fish_models


class ModelStorage:
    @classmethod
    def save_model(path, model, raycast_options, extra_options=None):
        with open(path) as f:
            assert isinstance(AbstractRaycastBasedModel, model)
            assert Raycast.option_keys in raycast_options
            pickle.dump([model, raycast_options, extra_options], f)

    @classmethod
    def load_model(path, validation_raycast_options):
        with open(path) as f:
            model, raycast_options, extra_option = pickle.load(f)
            assert (
                validation_raycast_options is None
                or validation_raycast_options == raycast_options
            )
            return model, raycast_options, extra_option


class AbstractRaycastBasedModel(abc.ABC):
    """
    Example was moved to AbstractModel
    """

    def __init_subclass__(cls, /, **kwargs):
        from warnings import warn

        warn(
            f"DeprecatedWarning: AbstractRaycastBasedModel is deprecated as\
            of 0.1 and will be removed in 0.3. The name of the class was\
            changed to AbstractModel. The class that inherited from\
            AbstractRaycastBasedModel is {cls}"
        )

    @abc.abstractmethod
    def choose_action(
        self, view: np.array  ## TODO Change to view array
    ) -> Tuple[float, float]:
        raise NotImplementedError


class AbstractModel(abc.ABC):
    @abc.abstractmethod
    def choose_action(self, **kwargs) -> Tuple[float, float]:
        """
        Pulls arguments via introspection on the argument names. To pull
        a certain argument during runtime, just write the corresponding
        named argument into the signature of your implementation of the
        choose_action method. Possible parameter names and what they pull
        are listed below.

        Possible Parameters
        ------------
        poses_3d : np.ndarray
            poses of all the guppies with shape (n_fishes, 3)
            Values in last dim are pos_x, pos_y and ori_in_radians
        poses_4d : np.ndarray
            poses of all the guppies with shape (n_fishes, 4)
            Values in last dim are pos_x, pos_y, ori_x and ori_y
        self_id : int
            index for the first dim of the poses_3d or poses_4d to find "you"
        view : np.ndarray
            FOV
        raycast : fish_models.gym_interface.Raycast
            The Raycast object that generated the view

        Returns
        --------
        speed : float
            speed in cm/s for move in the next time step
        turn : float
            turn speed in rad/s for move in the next time step


        Examples
        ---------
        TODO: Put example code to docs

        class ExampleGlobalKnowledgebasedModel(AbstractModel):
            def __init__(self, speed):
                self.speed = speed

            def choose_action(self, state_4d, self_id):
                previous_position = state_4d[self_id, :2]
                previous_direction = state_4d[self_id, :2]

                desired_direction = -previous_position

                return self.speed, turn(desired_direction, previous_direction)


        class ExamplePytorchConstantVelocityRaycastViewBasedModel(AbstractModel):
            def __init__(self, model: torch.nn.Module, speed, turn_angle_bin_values):
                self.model = model
                self.speed = speed

                rng = np.random.default_rng()

                def sample_turn_angle(multinomial_distribution):
                    turn_angle_bin_index = np.nonzero(
                        rng.multinomial(n=1, pvals=multinomial_distribution_turn_angle)
                    )[0][0]
                    return turn_angle_bin_values[turn_angle_bin_index]

                self.sample_turn_angle = sample_turn_angle

            def choose_action(self, view: np.ndarray) -> Tuple[float,float]:
                with torch.no_grad():
                    x = torch.Tensor(view).to(self.device)
                    output = self.model(x)
                    multinomial_distribution_turn_angle = torch.nn.functional.softmax(
                        output
                    ).detach().numpy()
                    turn_angle = sample_turn_angle(multinomial_distribution_turn_angle)
                return self.speed, turn_angle

        """
        raise NotImplementedError


class Raycast:
    option_keys = [
        "n_fish_bins",
        "n_wall_raycasts",
        "fov_angle_fish_bins",
        "fov_angle_wall_raycasts",
        "world_bounds",
        "far_plane",
    ]

    def __init__(
        self,
        n_fish_bins: int,
        n_wall_raycasts: int,
        fov_angle_fish_bins: float,
        fov_angle_wall_raycasts: float,
        world_bounds: Tuple[Tuple[float, float], Tuple[float, float]],
        far_plane: float = None,
    ):

        if far_plane is None:
            far_plane = np.sqrt(
                np.power((world_bounds[1][0] - world_bounds[0][0]), 2)
                + np.power((world_bounds[1][1] - world_bounds[0][1]), 2)
            )

        for k in Raycast.option_keys:
            assert locals()[k] is not None
            setattr(self, k, locals()[k])

        assert 0 <= fov_angle_fish_bins <= 2 * np.pi
        assert 0 <= fov_angle_wall_raycasts <= 2 * np.pi
        assert far_plane > 0

        # Prepare angles for raycasting. CANNOT directly be interpreted as FOV - see docstring
        def get_raycasts(n_rays, angle):
            return np.linspace(-angle / 2, angle / 2, num=n_rays, dtype=np.float32)

        self.wall_raycasts = get_raycasts(
            n_rays=n_wall_raycasts, angle=fov_angle_wall_raycasts
        )
        self.fish_sector_limits = get_raycasts(
            n_rays=n_fish_bins + 1, angle=fov_angle_fish_bins
        )

    def normalize_far_plane(self, distances):
        return 1 - np.clip(distances / self.far_plane, 0, 1)

    def cast_array(self, state_array):
        """Cast an array of states

        Args:
            state_array: An array of shape (fish, timesteps, [x, y, orientation_rad])
        Returns:
            Array of raycast tuples for the array (fish, timesteps, fish_bins + wall_raycasts)
        """
        n_fish, timesteps, fish_dim = state_array.shape
        assert (
            timesteps > n_fish
        ), "Do you really have more fish, than timesteps? If yes, please remove this assertion line locally."
        assert fish_dim == 3 or fish_dim == 4

        # print(state_array)
        result_cast = np.zeros(
            (n_fish, timesteps, self.n_fish_bins + self.n_wall_raycasts)
        )
        for fish_id in range(n_fish):
            for t in range(timesteps):
                result_cast[fish_id, t] = self.cast(state_array[:, t], fish_id)

        # print(result_cast.shape)
        return result_cast

    def cast(self, state, focal_fish_id) -> Tuple[np.ndarray, np.ndarray]:
        """
        Convert the state to FOV of other fish and walls via raycasting

        NOTE: world_bounds should use the same distance measure as your state
        also values in state should not exceed the world_bounds

        Parameters not already covered in generate_robofishIO_trackset_with_raycast_based_model
        ---------
        world_bounds : Tuple[float, float]
            Two Edges of the (rectangular) tank
        state : np.ndarray
            Absolute Positions/Orientations of all fish
            shape = (n_guppies, 3)
            state[:,:2] are x/y of Positions
            state[:,2] are the Orientations in rad

        Returns
        -------
        Tuple (fish_bins, wall_raycasts)
        each is an array with values from [0,1]
        value=0 means no fish/wall
        0<value<<1 means far away fish/wall
        0<<value<1 mean close fish/wall
        value=1 means fish/wall is where you are

        Each array can be interpreted as an INVERTED FieldOfView:
        Beginning of array / negative angles are "to the right" of the fish
        End of the array / positive angles are "to the left" of the fish
        If length of array is odd, the middle of the array is "straight ahead"

        # Questions you might ask yourself now:
        Why are negative (positive) angles to the right (left) of the fish?
        -> Look up the definition of radians, then it should make sense
        Why are the negative (positive) angles at the beginning (end) of the array?
        -> entity_entities_minimum_sector_distances can only digest them in that order

        TODO: Discussion about for a more intuive ordering should be part of the next
        generale robofish meeting
        """

        num_fishes, num_pose_parameters = state.shape
        if num_pose_parameters == 3:
            # Convert from (x, y, ori_rad) to shape (x, y, ori_x, ori_y)
            state_4d = np.concatenate(
                [state[:, :2], np.cos(state[:, 2:3]), np.sin(state[:, 2:3])], axis=1
            )
        elif num_pose_parameters == 4:
            state_4d = state
        else:
            raise Exception(
                "State dimensions have to be 3 (x,y,ori_rad) or 4 (x,y,ori_x, ori_y)"
            )

        # Calculate the view of other fishes
        # Note: If there is no other fish in a bin, the distance is set to np.inf
        views_of_other_fishes = robofish.core.entity_entities_minimum_sector_distances(
            entity_poses=state_4d,
            observer_index=focal_fish_id,
            sector_limit_angles=self.fish_sector_limits,
        )

        # Calculate the view of Walls
        views_of_walls = robofish.core.entity_boundary_distance(
            observer_pose=state_4d[focal_fish_id, :],
            boundary=self.world_bounds,
            ray_angles=self.wall_raycasts,
        )

        views_of_walls_norm = self.normalize_far_plane(views_of_walls)
        views_of_other_fishes_norm = self.normalize_far_plane(views_of_other_fishes)

        total_view = np.concatenate([views_of_other_fishes_norm, views_of_walls_norm])

        return total_view

    @property
    def options(self):
        o = {k: getattr(self, k) for k in Raycast.option_keys}
        return o


@deprecation.deprecated(
    deprecated_in="0.1",
    removed_in="0.3",
    details="The name of the class was changed to ModelGuppy.",
)
def RaycastBasedModelGuppy(*args, **kwargs):
    return ModelGuppy(*args, **kwargs)


class ModelGuppy(robofish.gym_guppy.guppies.TurnSpeedGuppy):
    """
    robofish.gym_guppy Guppy that can uses a model to choose actions. The
    model can basically be any python function, including a neural network

    Parameters
    ----------
    model : AbstractModel
        the model. Look at the docstring of AbstractModel for details
    world_conversion_factor : float
        robofish.gym_guppy always uses a world with side length 1. This
        conversion factor is for translation between that normalized space
        and a space of centimeters. The value corresponds to the side length
        of the tank in cm
    frequency : float
        spacing of the time steps in hz
    guppy_env : robofish.gym_guppy.envs.GuppyEnv
        environment
    raycast : fish_models.gym_interface.Raycast
        object for generating FOVs
    initial_pose : [float, float, float]
        initial x_pos, y_pos (in cm) and orientation (in rad)
    verbose : bool
        some printouts
    vaults : list of str
        enable/disable storing some values that are normally not saved.
        Mostly for debugging purposes
        current options: ["state", "view", "action"]
    """

    def __init__(
        self,
        model: AbstractModel,
        world_conversion_factor: float,
        frequency,
        guppy_env,
        raycast: Raycast = None,
        initial_pose=None,
        verbose=False,
        vaults=[],
    ):
        if initial_pose is None:
            position = np.random.uniform(low=-0.5, high=0.5, size=2)
            orientation = np.random.uniform(low=-np.pi, high=np.pi)

        else:
            position = initial_pose[:2]
            orientation = utils.limit_angle_range(
                initial_pose[2], _range=(-np.pi, np.pi)
            )

        assert (
            all(position > -0.5)
            and all(position < 0.5)
            and orientation > -np.pi
            and orientation < np.pi
        ), f"initial positions or orientations were not in range. Position (gym_world) {position} should be in range [-0.5, 0.5]. Orientation {orientation} should be in range [-pi, pi]"

        super().__init__(
            world=guppy_env.world,
            world_bounds=guppy_env.world_bounds,
            position=position,
            orientation=orientation,
        )

        self._max_turn = np.inf
        self._max_speed = np.inf

        self.model = model
        self.raycast = raycast
        self.world_conversion_factor = world_conversion_factor
        self.frequency = frequency
        self.verbose = verbose

        self.state_vault = [] if "state" in vaults else None
        self.view_vault = [] if "view" in vaults else None
        self.action_vault = [] if "action" in vaults else None

    def compute_next_action(self, state: np.ndarray, kd_tree, verbose=False):
        if self.state_vault is not None:
            self.state_vault.append(state)

        # State is [fishes, (x in m, y in m, ori in rad)]
        # The position is multiplied to achieve cm
        conv_state = np.copy(state)
        conv_state[:, :2] *= self.world_conversion_factor

        # Assemble parameters for model.choose_action
        kwargs = {}
        for parameter in inspect.signature(self.model.choose_action).parameters:

            if parameter == "poses_3d":
                kwargs[parameter] = conv_state
            elif parameter == "poses_4d":
                kwargs[parameter] = np.concatenate(
                    [
                        conv_state[:, :2],
                        np.cos(conv_state[:, 2:3]),
                        np.sin(conv_state[:, 2:3]),
                    ],
                    axis=1,
                )
            elif parameter == "self_id":
                kwargs[parameter] = self.id
            elif parameter == "view":
                view = self.raycast.cast(conv_state, self.id)
                kwargs[parameter] = view
                # Store the view
                if self.view_vault is not None:
                    self.view_vault.append(view)
            elif parameter == "raycast":
                kwargs[parameter] = self.Raycast
            else:
                raise RuntimeError(f"'{parameter}' not a possible parameter")

        speed_cm_per_second, turn_rad_per_second = self.model.choose_action(**kwargs)

        # Test if real speed turn is working
        # speed_cm_per_second, turn_rad_per_second = self.speeds_turns[self.count_step]
        # self.count_step += 1

        if self.verbose:
            view = view if "view" in locals() else None
            print(
                f"fish: {self.id} state: {conv_state} view: {view} speed (cm/s) {speed_cm_per_second} turn(rad/s) {turn_rad_per_second}"
            )

        # TurnSpeedGuppy expects turn per time step, so we divide by the frequency
        self.turn = turn_rad_per_second / self.frequency

        # TurnSpeedGuppy expects speed in m/s so we transfer it from cm/s
        # self.speed = speed_cm_per_second / self.world_conversion_factor
        self.speed = speed_cm_per_second / 100.0

        # Store the action
        if self.action_vault is not None:
            self.action_vault.append([speed_cm_per_second, turn_rad_per_second])

    def step(self, time_step):
        self.set_angular_velocity(0.0)
        super().step(time_step)

    @property
    def state_history(self):
        return np.stack(self.state_vault)

    @property
    def view_history(self):
        return np.stack(self.view_vault)

    @property
    def action_history(self):
        return np.stack(self.action_vault)


@deprecation.deprecated(
    deprecated_in="0.1",
    removed_in="0.3",
    details="The name of the class was changed to TrackGeneratorGym.",
)
def TrackGeneratorGymRaycast(*args, **kwargs):
    return TrackGeneratorGym(*args, **kwargs)


class TrackGeneratorGym:
    """
    Generate Track
    """

    def __init__(self, model, world_size, frequency=25, raycast=None):
        self.model, self.raycast, self.world_size, self.frequency = (
            model,
            raycast,
            world_size,
            frequency,
        )
        # Step 0: Sanity check if GuppyEnv class variables are still set as expected
        gym_world_width, gym_world_height = robofish.gym_guppy.envs.GuppyEnv.world_size
        assert (
            gym_world_width == gym_world_height == 1.0
        ), "world_size in GuppyEnv has changed, possibly breaking this function"

        # World has to be squared
        assert (
            np.std(world_size) == 0
        ), "World size has to be squared, this feature is prepared for support by guppy gym. If you need a rectangle world, please annoy mathis."

        self.world_conversion_factor = world_size[0] / gym_world_width

    def create_track(
        self,
        n_guppies,
        trackset_len,
        initial_poses=None,
        verbose=False,
        validation_dataset=None,
        histories=[],
    ):
        """
        Generate Track via robofish.gym_guppy

        Parameters
        ---------
        n_guppies : int
            Number of guppies to simulate
        trackset_len : int
            How many time steps of length 1/self.frequency should be simulated
            Note that for each time step, the compute_next_action of the model
            is going to be called exactly once
        initial_poses : np.ndarray of shape (n_guppies, 3), optional
            Provide initial poses. Default: Uniformly random poses
        verbose : bool, optional
            Additional prints. Default: False
        validation_dataset : bool, optional
            TODO implement sth that uses this
        histories : list of str, optional
            enable/disable storing some values that are normally not saved.
            Mostly for debugging purposes
            current options: ["state", "view", "action"]
        """
        generator: TrackGeneratorGym = self

        # TODO: This should not be neccessary, when env.get_agents works
        self.guppies = []

        # Convert the initial poses to the guppy gym world
        if initial_poses is not None:
            assert initial_poses.shape == (
                n_guppies,
                3,
            ), f"Initial pose had wrong shape: {initial_poses.shape}). Expected ({n_guppies} (n_guppies), 3 (x, y, ori_rad))"
            initial_poses = np.copy(initial_poses)
            initial_poses[:, :2] /= self.world_conversion_factor

        # Step 1.2: Create the Environment and add n_guppies ModelGuppys
        class Env(robofish.gym_guppy.envs.GuppyEnv):
            def _reset(self):
                for fish_id in range(n_guppies):
                    initial_pose = (
                        None if initial_poses is None else initial_poses[fish_id]
                    )
                    model = (
                        generator.model[fish_id]
                        if isinstance(generator.model, list)
                        else generator.model
                    )
                    if verbose:
                        print("model", model)
                        print(
                            "creating guppy with initial pose (coordinates in guppy world)",
                            initial_pose,
                        )

                    guppy = ModelGuppy(
                        model=model,
                        world_conversion_factor=generator.world_conversion_factor,
                        frequency=generator.frequency,
                        guppy_env=self,
                        initial_pose=initial_pose,
                        verbose=verbose,
                        raycast=generator.raycast,
                        vaults=histories,
                    )
                    self._add_guppy(guppy)
                    generator.guppies.append(guppy)

        time_step_s = 1 / self.frequency
        env = Env(
            time_step_s=time_step_s
        )  # _reset() is called here (in the __init__ of the superclass GuppyEnv)

        assert (
            env.sim_steps_per_second == 100
        ), "simulation step length in robofish.gym_guppy is fixed to 0.01s"
        assert (
            env._steps_per_action / env.sim_steps_per_second == time_step_s
        ), "env._steps_per_action/env.sim_steps_per_second is the number of seconds that pass in the simulation between calls of env.step(), so the lenght of a time step in seconds."
        assert (
            env._steps_per_action == env._guppy_steps_per_action
        ), "if both of these values are equal, one call of env.step() will also call all of the calculate_next_action() methods of the guppies exactly once. This is desirable for obvious reasons"

        # Step 2: Create Track via the step function of the Environment
        # Don't worry about action=None, it is ok b/c we have no robot
        initial_state = env.get_state()

        if verbose:
            print("Generating Track.")
        track = [initial_state]
        for _ in tqdm(range(trackset_len - 1)):
            # Note: env.step is NOT one simulation step
            states, *_ = env.step(action=None)
            track.append(states)

        track = np.stack(track, axis=0)

        assert track.shape == (
            trackset_len,
            n_guppies,
            3,
        ), f"track.shape: {track.shape}"

        # Step 3: Convert Track from guppy gym format to robofishIO format

        # Step 3.1: Wrap orientations to interval [0,2*pi) for robofishIO
        # Orientations are unbounded in guppy gym
        track[:, :, 2] = track[:, :, 2] % (2 * np.pi)

        # Step 3.2: Rescale x/y position values from m (guppy gym) to cm (robofishIO)
        # (0,0) is the middle of the tank for both guppy gym and robofishIO
        # So no other ajustments are needed to convert the positions
        track[:, :, :2] = track[:, :, :2] * self.world_conversion_factor

        # Step 3.3: Convert from shape TN3 (guppy gym) to shape NT3 (robofishIO)
        # Where T = trackset lenght, N = number of guppies
        track = np.swapaxes(track, 0, 1)

        return track

    @property
    def state_history(self):
        assert (np.std([g.state_history for g in self.guppies], axis=0) == 0).all()
        return np.swapaxes(self.guppies[0].state_history, 0, 1)

    @property
    def action_history(self):
        return np.stack([g.action_history for g in self.guppies])

    @property
    def view_history(self):
        return np.stack([g.view_history for g in self.guppies])

    @deprecation.deprecated(
        deprecated_in="0.1",
        removed_in="0.3",
        details="The name of the function was changed to as_io_file.",
    )
    def asIoFile(self, track, save_path=None, experiment_setup=None):
        return self.as_io_file(track, save_path, experiment_setup)

    def as_io_file(self, track, save_path=None, experiment_setup=None):

        # Save to IO file
        f = robofish.io.File(
            path=save_path,
            mode="x",  # fail if file already exists
            world_size_cm=self.world_size,
            frequency_hz=self.frequency,
        )
        if experiment_setup is not None:
            f.attrs["experiment_setup"] = experiment_setup

        f.create_multiple_entities(category="fish", poses=track)

        # Return the file
        return f
