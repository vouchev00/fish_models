import fish_models

from typing import Tuple
import numpy as np

np.set_printoptions(precision=5)


class BlindStochasticModel(fish_models.gym_interface.AbstractRaycastBasedModel):
    def __init__(self, tau=None):
        self.tau = tau
        self.training_data = None

    def train(self, dset):
        actions = dset[:]["actions"]
        speeds = actions[:, 0]
        turns = actions[:, 1]

        bins = 100
        max_speed = 20
        max_turn = 20

        speeds = np.clip(speeds, 0, max_speed)
        turns = np.clip(turns, -1 * max_turn, max_turn)

        self.speed_bins = np.linspace(0, max_speed, bins)
        self.turn_bins = np.linspace(-1 * max_turn, max_turn, bins)

        self.speed_hist = np.bincount(
            np.digitize(speeds, self.speed_bins), minlength=bins
        )[:bins]
        self.turn_hist = np.bincount(
            np.digitize(turns, self.turn_bins), minlength=bins
        )[:bins]

        self.speed_hist = np.array(self.speed_hist) / np.sum(self.speed_hist)
        self.turn_hist = np.array(self.turn_hist) / np.sum(self.turn_hist)

    def plot(self):
        import matplotlib.pyplot as plt

        plt.plot(self.speed_bins, self.speed_hist, label="speed histogram")
        plt.plot(self.turn_bins, self.turn_hist, label="turn histogram")
        plt.ylabel("Frequency")
        plt.legend()
        plt.show()

    def choose_action(
        self, view: np.ndarray, raycast: "fish_models.raycast.Raycast"
    ) -> Tuple[float, float]:
        speed = np.random.choice(self.speed_bins, p=self.speed_hist)
        turn = np.random.choice(self.turn_bins, p=self.turn_hist)

        return speed, turn
