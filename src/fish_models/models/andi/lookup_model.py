import fish_models

from typing import Tuple
import numpy as np

np.set_printoptions(precision=5)


class LookupModel(fish_models.gym_interface.AbstractRaycastBasedModel):
    def __init__(self, tau=None):
        self.tau = tau
        self.training_data = None

    def train(self, dset):
        self.views = dset["views"]
        self.actions = dset["actions"]

    def choose_action(self, view: np.ndarray) -> Tuple[float, float]:
        views_distance = self.views - view

        # print("distance\n", view_distance)
        # Normalize so that the distances to walls are less important
        # TODO: Test if this makes a difference
        views_distance[:, len(view) // 2 :] /= 6.0

        distances = np.linalg.norm(views_distance, axis=1)
        # print("distances norm\ņ", distances)
        if self.tau is None:
            choice = np.argmin(distances)
        else:
            distances = max(distances) - distances
            # distances /= sum(distances)

            distances_exp = np.exp(distances / self.tau)
            probabilities = distances_exp / np.sum(distances_exp)
            probabilities[np.isnan(probabilities)] = 0
            # print(
            #    f"Min: {min(probabilities):.3f}\tMean: {np.mean(probabilities):.3f}\tMax: {max(probabilities):.3f}\tBest: {np.round(np.sort(probabilities)[-10:],3)}"
            # )

            choice = np.random.choice(range(len(distances)), p=probabilities)
        return self.actions[choice]

        # print(distances.shape)

        # distances = max(distances) - distances
        # distances /= sum(distances)

        # distances_exp = np.exp(distances / self.tau)
        # probabilities = distances_exp / np.sum(distances_exp)
        # probabilities[np.isnan(probabilities)] = 0
        # print(
        #     min(probabilities),
        #     np.mean(probabilities),
        #     max(probabilities),
        #     np.sum(probabilities),
        #     np.sort(probabilities)[-10:],
        # )

        # action = np.random.choice(range(len(distances)), p=probabilities)

        # return self.training_data[action, -2:]
