import fish_models

from typing import Tuple
import numpy as np


class ReplayModel(fish_models.gym_interface.AbstractRaycastBasedModel):
    def __init__(self):
        pass

    def train(self, speeds_turns):
        self.speeds_turns = speeds_turns
        self.step = 0

    def choose_action(self, view: np.ndarray) -> Tuple[float, float]:
        speed_turn = self.speeds_turns[self.step]
        self.step += 1
        return speed_turn