from typing import List

import torch
import torch.nn as nn


# A Stack of LSTM Cells.
class LSTMBlock(nn.Module):
    """
    A Stack of LSTM Cells designed to work with most Captum Attribution Methods
    The main weirdness vs. a normal LSTM is the arrangement of arguments (batch size always in the front)
    TODO: Sadly, I did not find a way to make flexible hidden sizes work with captum :(
    The Problem is, that Captum does not support anything except Tensors as inputs (so no List[Tensor])
    But for flexible hidden size, you cannot combine the hidden states for different layers in a tensor (tensors can have only one size per dimension)
    """

    def __init__(self, input_size, hidden_sizes_list):
        super(LSTMBlock, self).__init__()

        assert all(
            [size == hidden_sizes_list[0] for size in hidden_sizes_list]
        ), "sadly captum does not combine well with flexible hidden sizes"

        self.input_size = input_size
        self.hidden_size = hidden_sizes_list[0]
        self.num_lstm_layers = len(hidden_sizes_list)
        assert self.num_lstm_layers >= 1

        self.input_hidden_sizes_list = [input_size] + hidden_sizes_list

        self.lstm_cell_stack = nn.ModuleList(
            [
                LSTMCell(input_size, hidden_size)
                for input_size, hidden_size in zip(
                    self.input_hidden_sizes_list[:-1], self.input_hidden_sizes_list[1:]
                )
            ]
        )

    def forward(self, input_, hidden_states, cell_states):
        x, hs, cs = input_, hidden_states, cell_states
        # print(x.shape) # should be(seq_num, self.input_size)
        # print(hs.shape,cs.shape) # should be (seq_num, self.num_lstm_layers, self.hidden_size)

        seq_num = x.shape[0]
        device = "cuda" if x.is_cuda else "cpu"

        next_hs = torch.zeros_like(hs)
        next_cs = torch.zeros_like(cs)

        # The output and the next hidden state of an lstm are the same
        # So there is no reason to get an extra variable for the input
        # x is basically the next_hs before next_hs[0]
        next_h = x

        for i, lstm_cell in enumerate(self.lstm_cell_stack):
            last_h, last_c = (
                hs[:, i],
                cs[:, i],
            )  # using this b/c zip does not work with nn.ModuleList

            next_h, next_c = lstm_cell(
                next_h, last_h, last_c
            )  # Note: next_h is defined outside loop

            next_hs[:, i] = next_h
            next_cs[:, i] = next_c

        return next_hs, next_cs


class LSTMCell(nn.Module):
    """
    An implementation of the image you see when you google "lstm cell"
    Mostly stolen from https://pytorch.org/blog/optimizing-cuda-rnn-with-torchscript/
    TODO maybe I can use the actual pytorch component here, just with batch_first=True?
    """

    def __init__(self, input_size, hidden_size):
        super(LSTMCell, self).__init__()

        self.input_size = input_size
        self.hidden_size = hidden_size

        # The weight/bias parameters for all four gates are combined to perform less calls to torch.mm
        self.weights_i = nn.Parameter(torch.randn(4 * hidden_size, input_size))
        self.weights_h = nn.Parameter(torch.randn(4 * hidden_size, hidden_size))
        # init forget gate bias to 1, to remember everything by default
        self.biases = nn.Parameter(
            torch.cat(
                (
                    torch.zeros(1 * hidden_size),
                    torch.ones(1 * hidden_size),
                    torch.zeros(2 * hidden_size),
                )
            )
        )
        # The pytorch implementation has 2 biases fur CuDNN compatibility. TODO: Try if this is bettwer/worse now

        self.layer_norm_in = nn.LayerNorm(hidden_size)
        self.layer_norm_forget = nn.LayerNorm(hidden_size)
        self.layer_norm_cell = nn.LayerNorm(hidden_size)
        self.layer_norm_out = nn.LayerNorm(hidden_size)

    def forward(self, input_, hidden_state, cell_state):
        x, h, c = input_, hidden_state, cell_state
        # print(x.shape) # should be (seq_num, self.input_size)
        # print(h.shape,c.shape) # should be (seq_num, sefl.hidden_size)

        # Do all of the multiplcation at once b/c that is how the cool kids do
        gates = (
            torch.mm(x, self.weights_i.t())
            + torch.mm(h, self.weights_h.t())
            + self.biases
        )

        ingate, forgetgate, cellgate, outgate = gates.chunk(
            4, 1
        )  # careful when changing this, second pos is reserved for forget gate

        # Layer Norm and Activation Function
        ingate = torch.sigmoid(self.layer_norm_in(ingate))
        forgetgate = torch.sigmoid(self.layer_norm_forget(forgetgate))
        cellgate = torch.tanh(self.layer_norm_cell(cellgate))
        outgate = torch.sigmoid(self.layer_norm_out(outgate))

        next_c = (forgetgate * c) + (ingate * cellgate)
        next_h = outgate * torch.tanh(next_c)

        return next_h, next_c
