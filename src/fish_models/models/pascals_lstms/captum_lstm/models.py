from typing import List

import torch
import torch.nn as nn
from fish_models.models.pascals_lstms.captum_lstm.lstm_base import LSTMBlock

"""
An LSTM designed to work with most Captum attribution Methods
The main weirdness vs. a normal LSTM is the arrangement of arguments
"""


class TurnAnglePredictionCaptumFishLSTM(nn.Module):
    """
    Takes 2 FOV Vectors as inputs (= input has 2 channels)
    First puts inputs through some conv/fc layers
    Then puts them through some LSTM cells
    Then puts them through some more fc layers
    At the end, gives
    The outputs represent change_in_speed and change_in_rotation
    """

    # def __init__(self, input_bin_num, lstm_hidden_size, num_lstm_layers):
    def __init__(
        self,
        input_shape,
        conv_channel_sizes_list,
        lstm_hidden_sizes_list,
        output_fc_sizes_list,
        output_shape,
        p_dropout=0.2,
    ):
        super(TurnAnglePredictionCaptumFishLSTM, self).__init__()

        self.input_channels, self.input_bin_num = input_shape
        (
            self.output_channels,
            self.output_bin_num,
        ) = output_shape  # TODO: Make this actuall work

        conv_in_out_list = [
            c_in_out
            for c_in_out in zip(
                [self.input_channels] + conv_channel_sizes_list[:-1],
                conv_channel_sizes_list,
            )
        ]

        def get_conv(in_channels, out_channels):
            return nn.Conv1d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=3,
                padding=1,
            )

        self.input_conv = nn.Sequential(
            *[
                module(c_in, c_out)
                for (c_in, c_out) in conv_in_out_list
                for module in (get_conv, lambda _, __: nn.ReLU())
            ]
        )

        self.flatten = nn.Flatten(start_dim=1, end_dim=-1)

        self.lstm_block = LSTMBlock(
            input_size=self.input_bin_num * conv_channel_sizes_list[-1],
            hidden_sizes_list=lstm_hidden_sizes_list,
        )

        fc_in_out_list = [
            neurons_in_out
            for neurons_in_out in zip(
                [lstm_hidden_sizes_list[-1]] + output_fc_sizes_list[:-1],
                output_fc_sizes_list,
            )
        ]
        self.output_fc = nn.Sequential(
            *[
                module(n_in, n_out)
                for (n_in, n_out) in fc_in_out_list
                for module in (
                    nn.Linear,
                    lambda _, __: nn.Dropout(p=p_dropout),
                    lambda _, __: nn.ReLU(),
                )
            ]
        )

        # TODO: This could be extended to predict turn angle AND speed
        # Note: The output are logits to be used with softmax corssentropy loss
        self.output_layer = nn.Sequential(
            nn.Linear(output_fc_sizes_list[-1], self.output_bin_num)
        )

        # Learnable Initial hidden and cell states
        self.init_hs = nn.Parameter(
            torch.zeros(
                (1, self.lstm_block.num_lstm_layers, self.lstm_block.hidden_size)
            )
        )
        self.init_cs = nn.Parameter(
            torch.zeros(
                (1, self.lstm_block.num_lstm_layers, self.lstm_block.hidden_size)
            )
        )

    def forward(self, input_sequence):
        xs = input_sequence
        # print(xs.shape) # should be (seq_num, seq_length, self.input_bin_num)

        seq_num, seq_length = xs.shape[0], xs.shape[1]

        hs, cs = self.init_hs.repeat((seq_num, 1, 1)), self.init_cs.repeat(
            (seq_num, 1, 1)
        )
        # print(hs.shape,cs.shape) # should be (seq_num, self.lstm_block.num_lstm_layers, self.lstm_block.hidden_size)

        device = "cuda" if xs.is_cuda else "cpu"
        ys = torch.zeros(size=(seq_num, seq_length, self.output_bin_num), device=device)

        for i in range(seq_length):
            cell_input = self.flatten(self.input_conv(xs[:, i]))
            hs, cs = self.lstm_block(cell_input, hs, cs)
            # TODO: One could easily implement a "skip connection" here by summing over hs instead of using the last one
            ys[:, i] = self.output_layer(self.output_fc(hs[:, -1]))

        return ys


class ToyLSTM(nn.Module):
    """
    A small custom LSTM wrapper for simple test and toy experiments
    """

    def __init__(self, num_lstm_layers, input_size, hidden_size, output_size):
        super(ToyLSTM, self).__init__()

        self.input_size = input_size
        self.output_size = output_size

        self.lstm_block = LSTMBlock(
            input_size=input_size, hidden_sizes_list=[hidden_size] * num_lstm_layers
        )

        # Learnable Initial hidden and cell states
        self.init_hs = nn.Parameter(
            torch.zeros(
                (1, self.lstm_block.num_lstm_layers, self.lstm_block.hidden_size)
            )
        )
        self.init_cs = nn.Parameter(
            torch.zeros(
                (1, self.lstm_block.num_lstm_layers, self.lstm_block.hidden_size)
            )
        )

        assert hidden_size // 4 >= output_size
        self.output_layer = nn.Sequential(
            nn.Linear(hidden_size, hidden_size // 2),
            nn.Sigmoid(),
            nn.Linear(hidden_size // 2, hidden_size // 4),
            nn.Sigmoid(),
            nn.Linear(hidden_size // 4, output_size),
        )

    def forward(self, input_sequence):
        xs = input_sequence
        # print(xs.shape) # should be (seq_num, seq_length, input_size)

        seq_num, seq_length = xs.shape[0], xs.shape[1]

        hs, cs = self.init_hs.repeat((seq_num, 1, 1)), self.init_cs.repeat(
            (seq_num, 1, 1)
        )
        # print(hs.shape,cs.shape) # should be (seq_num, self.lstm_block.num_lstm_layers, self.lstm_block.hidden_size)

        device = "cuda" if xs.is_cuda else "cpu"
        ys = torch.zeros(size=(seq_num, seq_length, self.output_size), device=device)

        for i in range(seq_length):
            hs, cs = self.lstm_block(xs[:, i], hs, cs)
            ys[:, i] = self.output_layer(hs[:, -1])

        return ys


class ToyLSTMManyToOneClassification(nn.Module):
    """
    A small custom LSTM wrapper for simple test and toy experiments
    """

    def __init__(self, num_lstm_layers, input_size, hidden_size, classes_num):
        super(ToyLSTMManyToOneClassification, self).__init__()

        self.input_size = input_size

        self.lstm_block = LSTMBlock(
            input_size=input_size, hidden_sizes_list=[hidden_size] * num_lstm_layers
        )

        # Learnable Initial hidden and cell states
        self.init_hs = nn.Parameter(
            torch.zeros(
                (1, self.lstm_block.num_lstm_layers, self.lstm_block.hidden_size)
            )
        )
        self.init_cs = nn.Parameter(
            torch.zeros(
                (1, self.lstm_block.num_lstm_layers, self.lstm_block.hidden_size)
            )
        )

        assert hidden_size // 4 >= classes_num
        self.output_layer = nn.Sequential(
            nn.Linear(hidden_size, hidden_size // 2),
            nn.Sigmoid(),
            nn.Linear(hidden_size // 2, hidden_size // 4),
            nn.Sigmoid(),
            nn.Linear(hidden_size // 4, classes_num),
        )

    def forward(self, input_sequence):
        xs = input_sequence
        # print(xs.shape) # should be (seq_num, seq_length, input_size)

        seq_num, seq_length = xs.shape[0], xs.shape[1]

        hs, cs = self.init_hs.repeat((seq_num, 1, 1)), self.init_cs.repeat(
            (seq_num, 1, 1)
        )
        # print(hs.shape,cs.shape) # should be (seq_num, self.lstm_block.num_lstm_layers, self.lstm_block.hidden_size)

        for i in range(seq_length):
            hs, cs = self.lstm_block(xs[:, i], hs, cs)

        return self.output_layer(hs[:, -1])
