import time

import torch


# stolen from https://stackoverflow.com/questions/59584457/pytorch-is-there-a-definitive-training-loop-similar-to-keras-fit
def train_lstm_with_tbptt(
    model,
    optimizer,
    train_dl,
    val_dl,
    epochs=20,
    print_every=1,
    save_every=None,
    save_path=None,
    tbptt_n_labels=1,
    tbptt_n_inputs=10,
    tbptt_step_len=10,
    ce_weights=None,
    device="cpu",
):
    """
    Trains an LSTM using Truncated Backpropagation Through Time (tbptt).
    Returns Keras-style per-epoch history of loss and accuracy over training and validation data.

    NOTE: Assumes learnable initial hidden state (TODO: Use passable hidden state. This will improve training speed!)
    NOTE: Assumes all batches have the same size (e.g. thorugh drop_last = True for the dataloaders)

    Parameters
    ----------
    model : nn.Module
        Neural network model
    optimizer : torch.optim.Optimizer
        Search space optimizer (e.g. Adam)
    train_dl :
        Iterable dataloader for training data.
    val_dl :
        Iterable dataloader for validation data.
    epochs : int
        Number of epochs to run
    print_every : int
        Only print outputs after every print_every-th epoch
    save_every : int
        Only save models state dict after every save_every-th epoch. Does nothing if save_path = None
    save_path : string
        Path to directory where to save the models state dict
    tbptt_n_labels : int
        How many labels are used to compute for each weight update
        tbptt_n_labels ==  tbptt_n_inputs -> this is the Many-to-Many case
        tbptt_n_labels == tbptt_step_len -> No labels will be skipped
        tbptt_n_labels ==  1 -> this is the Many-to-One case
        In this function, this parameter is called k3
    tbptt_n_inputs : int
        tbptt_n_inputs - tbptt_step_len == size of input overlap
        How many inputs are considered for each weight update
        In the standard BPTT this is the parameter k2
    tbptt_step_len : int
        How far to jump in the sequence after one weight update
        In the standard BPTT this is the parameter k1
    ce_weights : torch.Tensor
        Manual Rescaling weight given to each class. Default: None
    device : string
        Specifies 'cuda' or 'cpu'

    Returns
    -------
    Dictionary
        Similar to Keras' fit(), the output dictionary contains per-epoch
        history of training loss, training accuracy, validation loss, and
        validation accuracy.
    """

    print(
        "train() called: model=%s, opt=%s(lr=%f), epochs=%d, device=%s\n"
        % (
            type(model).__name__,
            type(optimizer).__name__,
            optimizer.param_groups[0]["lr"],
            epochs,
            device,
        )
    )

    history = {}  # Collects per-epoch loss and acc like Keras' fit().
    history["loss"] = []
    history["acc"] = []
    history["val_loss"] = []
    history["val_acc"] = []

    start_time_sec = time.time()

    model.to(device)

    ce_loss = torch.nn.CrossEntropyLoss(
        weight=ce_weights, reduction="mean"
    )  # TODO: Insert weights based on data?

    k1, k2, k3 = tbptt_step_len, tbptt_n_inputs, tbptt_n_labels
    assert k2 >= k3

    for epoch in range(epochs):

        # --- TRAIN AND EVALUATE ON TRAINING SET -----------------------------
        model.train()
        train_count = 0
        train_loss = 0.0
        num_train_correct = 0

        for mini_batch in train_dl:
            optimizer.zero_grad()

            x = mini_batch[0].to(device)
            y = mini_batch[1].to(device)

            batch_size = x.shape[0]
            sequence_length = x.shape[1]

            for t in range(0, sequence_length - k2, k1):
                yhat = model(x[:, t : t + k2])
                # TODO: change when channel output is added
                # print(yhat.shape) # should be (batch_size, k1, model.output_bins)
                yhat = yhat[
                    :, -k3:
                ]  # Only the last k3 outputs are considered in the loss
                yhat.transpose_(1, -1)  # CE Loss needs shape (batch_size, C, ...)
                yt = y[
                    :, t + k2 - k3 : t + k2
                ]  # The corresponding labels for the current t
                # print(yhat.shape, yt.shape)
                loss = ce_loss(yhat, yt)  # TODO: Also this

                loss.backward()
                optimizer.step()

                train_count += batch_size
                train_loss += loss.data.item()
                # How many predictions were correct.
                num_train_correct += (torch.max(yhat, dim=1)[1] == yt).sum().item()

        train_acc = num_train_correct / train_count / k3
        train_loss = train_loss / train_count

        # --- EVALUATE ON VALIDATION SET -------------------------------------
        model.eval()
        val_count = 0
        val_loss = 0.0
        num_val_correct = 0

        with torch.no_grad():

            for mini_batch in val_dl:

                x = mini_batch[0].to(device)
                y = mini_batch[1].to(device)

                batch_size = x.shape[0]
                sequence_length = x.shape[1]

                for t in range(0, sequence_length - k2, k1):
                    yhat = model(x[:, t : t + k2])
                    # TODO: change when channel output is added
                    # print(yhat.shape) # should be (batch_size, k1, model.output_bins)
                    yhat = yhat[
                        :, -k3:
                    ]  # Only the last k3 outputs are considered in the loss
                    yhat.transpose_(1, -1)  # CE Loss needs shape (batch_size, C, ...)
                    yt = y[
                        :, t + k2 - k3 : t + k2
                    ]  # The corresponding labels for the current t
                    loss = ce_loss(yhat, yt)  # TODO: Also this

                    val_count += batch_size
                    val_loss += loss.data.item()
                    # How many predictions were correct.
                    num_val_correct += (torch.max(yhat, dim=1)[1] == yt).sum().item()

        val_acc = num_val_correct / val_count / k3
        val_loss = val_loss / val_count

        if ((epoch + 1) % print_every == 0) or ((epoch + 1) == epochs):
            print(
                "Epoch %3d/%3d, train loss: %5.3f, val loss: %5.3f, train acc: %5.3f, val acc: %5.3f "
                % (epoch + 1, epochs, train_loss, val_loss, train_acc, val_acc)
            )

        history["loss"].append(train_loss)
        history["acc"].append(train_acc)
        history["val_loss"].append(val_loss)
        history["val_acc"].append(val_acc)

        if save_every and ((epoch + 1) % save_every == 0) or ((epoch + 1) == epochs):
            torch.save(model.state_dict(), f"{save_path}statedict_epoch{epoch + 1}.pt")

    # END OF TRAINING LOOP

    end_time_sec = time.time()
    total_time_sec = end_time_sec - start_time_sec
    time_per_epoch_sec = total_time_sec / epochs
    print()
    print("Time total:     %5.2f sec" % (total_time_sec))
    print("Time per epoch: %5.2f sec" % (time_per_epoch_sec))

    return history
