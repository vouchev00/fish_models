import time
import torch


# stolen from https://stackoverflow.com/questions/59584457/pytorch-is-there-a-definitive-training-loop-similar-to-keras-fit
def train_lstm(
    model,
    optimizer,
    loss_fn,
    train_dl,
    val_dl,
    epochs=20,
    print_every=1,
    save_every=None,
    save_path=None,
    device="cpu",
):
    """
    Runs training loop for classification problems. Returns Keras-style
    per-epoch history of loss and accuracy over training and validation data.

    Parameters
    ----------
    model : nn.Module
        Neural network model
    optimizer : torch.optim.Optimizer
        Search space optimizer (e.g. Adam)
    loss_fn :
        Loss function (e.g. nn.CrossEntropyLoss())
    train_dl :
        Iterable dataloader for training data.
    val_dl :
        Iterable dataloader for validation data.
    epochs : int
        Number of epochs to run
    print_every : int
        Only print outputs after every print_every-th epoch
    save_every : int
        Only save models state dict after every save_every-th epoch. Does nothing if save_path = None
    save_path : string
        Path to directory where to save the models state dict
    device : string
        Specifies 'cuda' or 'cpu'

    Returns
    -------
    Dictionary
        Similar to Keras' fit(), the output dictionary contains per-epoch
        history of training loss, training accuracy, validation loss, and
        validation accuracy.
    """

    print(
        "train() called: model=%s, opt=%s(lr=%f), epochs=%d, device=%s\n"
        % (
            type(model).__name__,
            type(optimizer).__name__,
            optimizer.param_groups[0]["lr"],
            epochs,
            device,
        )
    )

    history = {}  # Collects per-epoch loss and acc like Keras' fit().
    history["loss"] = []
    history["val_loss"] = []

    start_time_sec = time.time()

    model.to(device)

    for epoch in range(epochs):

        # --- TRAIN AND EVALUATE ON TRAINING SET -----------------------------
        model.train()
        train_loss = 0.0

        for mini_batch in train_dl:
            optimizer.zero_grad()

            x = mini_batch[0].to(device)
            y = mini_batch[1].to(device)
            yhat = model(x)
            loss = loss_fn(yhat, y)

            loss.backward()
            optimizer.step()

            train_loss += loss.data.item() * x.size(0)

        train_loss = train_loss / len(train_dl.dataset)

        # --- EVALUATE ON VALIDATION SET -------------------------------------
        model.eval()
        val_loss = 0.0

        for mini_batch in val_dl:
            x = mini_batch[0].to(device)
            y = mini_batch[1].to(device)
            yhat = model(x)
            loss = loss_fn(yhat, y)

            val_loss += loss.data.item() * x.size(0)

        val_loss = val_loss / len(val_dl.dataset)

        if ((epoch + 1) % print_every == 0) or ((epoch + 1) == epochs):
            print(
                "Epoch %3d/%3d, train loss: %5.3f, val loss: %5.3f"
                % (epoch + 1, epochs, train_loss, val_loss)
            )

        history["loss"].append(train_loss)
        history["val_loss"].append(val_loss)

        if save_every and ((epoch + 1) % save_every == 0) or ((epoch + 1) == epochs):
            torch.save(model.state_dict(), f"{save_path}statedict_epoch{epoch + 1}.pt")

    # END OF TRAINING LOOP

    end_time_sec = time.time()
    total_time_sec = end_time_sec - start_time_sec
    time_per_epoch_sec = total_time_sec / epochs
    print()
    print("Time total:     %5.2f sec" % (total_time_sec))
    print("Time per epoch: %5.2f sec" % (time_per_epoch_sec))

    return history
