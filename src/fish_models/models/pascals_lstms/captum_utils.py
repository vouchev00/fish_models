import numpy as np
import torch
from captum.attr import IntegratedGradients, Saliency, GuidedBackprop
import matplotlib.pyplot as plt


class CaptumLSTMInputAttributer:
    # TODO: Implement One-Hot flexible targets
    def __init__(self, model, dataloader, device, name_extra):
        self.model = model
        self.dataloader = dataloader
        self.device = device
        self.name_extra = name_extra
        self.last_attr = None
        self.last_attr_name = None

    def __attribution_loop__(self, use_method, method_name):
        assert (
            len(self.dataloader.dataset[0][0].shape) == 3
        )  # Only implemented for 1D inputs atm
        seq_num = self.dataloader.dataset[0][0].shape[0]
        subsequence_num = len(self.dataloader)
        subsequence_length = self.dataloader.batch_size
        input_size = self.dataloader.dataset[0][0].shape[2]

        attr_inp = np.zeros((subsequence_num, seq_num, subsequence_length, input_size))

        for i, (x, y) in enumerate(self.dataloader):
            x.requires_grad = True
            x = x.to(self.device)
            init_hidden_states = torch.zeros(
                seq_num,
                self.model.lstm_block.num_lstm_layers,
                self.model.lstm_block.hidden_size,
                requires_grad=True,
                device=self.device,
            )
            init_cell_states = torch.zeros(
                seq_num,
                self.model.lstm_block.num_lstm_layers,
                self.model.lstm_block.hidden_size,
                requires_grad=True,
                device=self.device,
            )
            attr = use_method(inputs=(x, init_hidden_states, init_cell_states))
            attr_inp[i] = attr[0].detach().numpy()

        self.last_attr = attr_inp
        self.last_attr_name = method_name
        return self

    def show_mean_importance(self, for_last_timesteps):
        (
            subsequence_num,
            seq_num,
            subsequence_length,
            input_size,
        ) = self.last_attr.shape  # Only implemented for 1D inputs atm
        attr_inp_mean = self.last_attr.reshape(-1, subsequence_length, input_size).mean(
            0
        )

        for input_category in range(input_size):
            plt.bar(
                range(-for_last_timesteps + 1, 1, 1),
                attr_inp_mean[
                    subsequence_length - for_last_timesteps :, input_category
                ],
                label=f"input {input_category}",
            )
        plt.title(
            f"{self.last_attr_name}. {for_last_timesteps}/{subsequence_length} timesteps. {self.name_extra}"
        )
        plt.plot()
        plt.show()

    def IntegratedGradients(self, target, baselines=None):
        def use(inputs):
            self.model.eval()
            self.model.to(self.device)
            method = IntegratedGradients(self.model)
            if baselines:
                return method.attribute(
                    inputs, target=target, baselines=baselines
                )  # target is the "target class index"
            else:
                return method.attribute(
                    inputs, target=target
                )  # target is the "target class index"

        return self.__attribution_loop__(
            use_method=use, method_name=IntegratedGradients.get_name()
        )

    def Saliency(self, target):
        def use(inputs):
            self.model.eval()
            self.model.to(self.device)
            method = Saliency(self.model)
            return method.attribute(
                inputs, target=target
            )  # target is the "target class index"

        return self.__attribution_loop__(
            use_method=use, method_name=Saliency.get_name()
        )

    def GuidedBackprop(self, target):
        def use(inputs):
            self.model.eval()
            self.model.to(self.device)
            method = GuidedBackprop(self.model)
            return method.attribute(
                inputs, target=target
            )  # target is the "target class index"

        return self.__attribution_loop__(
            use_method=use, method_name=GuidedBackprop.get_name()
        )
