from functools import partial

import numpy as np

import fish_models.utils
from fish_models.gym_interface import AbstractModel


class ConstantSpeedCouzinModel(AbstractModel):
    def __init__(
        self,
        speed_cm_s,
        zor,
        zoo,
        zoa,
        zowr=0.0,
        world_bounds=[[-50, -50], [50, 50]],
        max_angular_velocity_rad_s=None,
        fov=None,
        neighbours=None,
        time_step_s=1 / 25,
        noise=None,
    ):
        assert zor <= zoo <= zoa
        self.zor, self.zoo, self.zoa = zor, zoo, zoa

        self.x_lims = world_bounds[1][0] - zowr, world_bounds[0][1] + zowr
        self.y_lims = world_bounds[1][1] - zowr, world_bounds[0][0] + zowr
        assert self.x_lims[0] > self.x_lims[1]
        assert self.y_lims[0] > self.y_lims[1]

        self.speed = speed_cm_s
        self.turn = partial(
            fish_models.utils.turn_angular_velocity,
            time_step_s=time_step_s,
            noise=noise,
            max_angular_velocity_rad_s=max_angular_velocity_rad_s,
        )

        self.fov = fov
        self.neighbours = neighbours

    def wall_repulsion(self, pos: np.ndarray):
        # Walls are checked in the following order: +X, +Y, -X, -Y
        x, y = pos[0], pos[1]
        too_close = [
            pos[0] > self.x_lims[0],
            pos[1] > self.y_lims[0],
            pos[0] < self.x_lims[1],
            pos[1] < self.y_lims[1],
        ]
        if any(too_close):
            repulsion_vectors = np.array([[-1, 0], [0, -1], [1, 0], [0, 1]])
            active_repulsion_vectors = repulsion_vectors[too_close]
            return active_repulsion_vectors.sum(axis=0)

        else:
            return None

    def choose_action(self, poses_4d, self_id):
        assert poses_4d.shape[1] == 4
        n_fishes = poses_4d.shape[0]

        own_position = poses_4d[self_id, :2]
        own_orientation = poses_4d[self_id, 2:]

        # Case 1: Fish close to Wall
        wall_repulsion_vector = self.wall_repulsion(own_position)
        if wall_repulsion_vector is not None:
            return self.speed, self.turn(
                previous_direction=own_orientation,
                desired_direction=wall_repulsion_vector,
            )

        # For all other cases, some additional calculations are required

        # Calculate distances and (unit) relative position vectors
        relative_positions = poses_4d[:, :2] - own_position
        distances = np.linalg.norm(relative_positions, axis=1, keepdims=True)
        relative_positions_unit = np.divide(
            relative_positions, distances, where=distances != 0
        )
        distances = np.squeeze(distances)  # extra dim not needed after here

        # Construct boolean mask to ignore fishes
        in_fov = np.full(n_fishes, True)
        in_fov[self_id] = False  # self is never in fov
        if self.fov is not None:
            turn_angles = fish_models.utils.turn_angle(
                np.tile(own_orientation, (n_fishes, 1)).T, relative_positions.T
            )
            in_fov[np.where(np.abs(turn_angles) > self.fov / 2)] = False
        if self.neighbours is not None:
            in_fov[np.argsort(distances)[self.neighbours + 1 :]] = False

        # Case 2: Fish are in ZOR
        in_zor = (distances <= self.zor) & in_fov
        if any(in_zor):
            d_r = -np.sum(relative_positions_unit[in_zor], axis=0)
            return self.speed, self.turn(
                previous_direction=own_orientation, desired_direction=d_r
            )

        # Case 3: Fish are in ZOO and/or ZOA
        in_zoo = (self.zor < distances) & (distances <= self.zoo) & in_fov
        in_zoa = (self.zoo < distances) & (distances <= self.zoa) & in_fov
        if any(in_zoo) or any(in_zoa):
            d_o = np.sum(poses_4d[:, 2:][in_zoo], axis=0)
            d_a = np.sum(relative_positions_unit[in_zoa], axis=0)
            return self.speed, self.turn(
                previous_direction=own_orientation, desired_direction=d_o + d_a
            )

        # Case 4: No other fish
        return self.speed, self.turn(
            previous_direction=own_orientation, desired_direction=np.zeros(2)
        )
