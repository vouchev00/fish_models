import pathlib

import numpy as np
import robofish.io
import torch
from fish_models.models.pascals_lstms.data_manipulation.preprocessing import (
    transform_poses_to_fov,
)

import fish_models
from deprecation import deprecated


@deprecated("Please use io_datasets")
class TurnAnglePredictionDataset(torch.utils.data.Dataset):
    """
    Many-to-one Turn Angle Prediction Dataset

    Datapoints are a input/label tuples of shape
    ((sequence_length, 2, n_fov_raycasts), (1,))

    Args:
        data_dir: Path to the directory with the .hdf5 files
        n_fov_raycasts: Granularity of the Input
        n_turn_angle_bins: Granularity of the Output
        fov_angle_rad: How much the fish can see. Default: 2pi (sees 360 degrees / everything)
        max_turn_angle_rad: How far the fish can turn. Deafault: 1pi (max. 180 degrees turn per time step)
        glob: Regex to find only specifically named .hdf5 Files. Default: Finds all .hdf5 Files in data_dir
    """

    def __init__(
        self,
        data_dir,
        n_fov_raycasts,
        n_turn_angle_bins,
        fov_angle_rad=2 * np.pi,
        max_turn_angle_rad=np.pi,
        glob="*.hdf5",
    ):
        wall_raycasts_num = fish_sectors_num = n_fov_raycasts

        self.input_sequences = []
        self.label_sequences = []
        self.poses_sequences = []

        for path in pathlib.Path(data_dir).glob(glob):
            with robofish.io.File(path=path) as f:
                # Grab Poses from .hdf5 File # TODO: There will be an IO function that loads multiple poses at once, use that here instead
                poses = f.entity_poses_rad
                num_fishes_in_trackset, trackset_len, num_pose_parameters = poses.shape
                assert num_pose_parameters == 3

                # Generate Inputs (= FOVs of the fish)

                world_size_x, world_size_y = f.world_size
                assert world_size_x == world_size_y, "only quadratic worlds allowed"
                world_bounds = (
                    (-world_size_x / 2, -world_size_y / 2),
                    (world_size_x / 2, world_size_y / 2),
                )
                far_plane = np.linalg.norm(
                    [world_size_x, world_size_y]
                )  # diagonal of square

                # Wall raycasts should be in the middle of the fish bins
                # So we cut half a bin size from each edge of the FOV
                fov_angle_raycasts_rad = (
                    fov_angle_rad * wall_raycasts_num / (wall_raycasts_num + 1)
                )

                raycaster = fish_models.gym_interface.Raycast(
                    n_fish_bins=fish_sectors_num,
                    n_wall_raycasts=wall_raycasts_num,
                    fov_angle_fish_bins=fov_angle_rad,
                    fov_angle_wall_raycasts=fov_angle_raycasts_rad,
                    world_bounds=world_bounds,
                    far_plane=far_plane,
                )

                vow = np.zeros(
                    (num_fishes_in_trackset, trackset_len, wall_raycasts_num),
                    dtype=np.float32,
                )
                vof = np.zeros(
                    (num_fishes_in_trackset, trackset_len, fish_sectors_num),
                    dtype=np.float32,
                )

                vow, vof = (
                    vow[:, :-1],
                    vof[:, :-1],
                )  # there is no label for the last input

                # Calculate vowalls and vofishes via raycasting
                # Note: If there is no other fish in a bin, the distance is set to np.inf
                for i in range(num_fishes_in_trackset):
                    for t in range(trackset_len - 1):
                        vow[i, t], vof[i, t] = raycaster.cast(
                            state=poses[:, t], focal_fish_id=i
                        )

                # Generate Labels (= turn angle bin indices)
                # TODO this is copied from develop branch of IO, replace by IO function when released

                turn_angles = f.entity_speeds_turns[:, :, 1]
                turn_angles = np.clip(
                    turn_angles, -max_turn_angle_rad, max_turn_angle_rad
                )

                turn_bin_borders = np.linspace(
                    -max_turn_angle_rad,
                    max_turn_angle_rad,
                    n_turn_angle_bins + 1,
                    dtype=np.float32,
                )

                turn_angles_binned = (
                    np.digitize(turn_angles, turn_bin_borders) - 1
                )  # -1 is to make real class labels from [0, C-1] out of the bin indices [1, C]
                assert np.all(turn_angles_binned >= 0)
                assert np.all(turn_angles_binned < n_turn_angle_bins)

                # Store Inputs and Labels
                for i in np.arange(num_fishes_in_trackset):
                    self.input_sequences.append(
                        np.float32(np.stack([vow[i], vof[i]], axis=1))
                    )  # This will likely change
                    self.label_sequences.append(
                        np.vectorize(np.long)(turn_angles_binned[i])
                    )

                # Store poses for attribution uses
                # The First Element in the Tuple tells you where the poses are saved
                self.poses_sequences.append((0, poses[:, :-1, :]))
                for i in range(1, num_fishes_in_trackset):
                    self.poses_sequences.append((i, None))

    def __len__(self):
        return len(self.input_sequences)

    def __getitem__(self, index):
        return self.input_sequences[index], self.label_sequences[index]

    def get_poses_and_focal_id(self, index):
        focal_id = self.poses_sequences[index][0]
        assert self.poses_sequences[index - focal_id][1] is not None
        return focal_id, self.poses_sequences[index - focal_id][1]


class ToyShiftedRandomSequenceDataset(torch.utils.data.Dataset):
    """
    Basic idea: Each label is sth the network has seen before as an input.
    Note: Proably worse if subsequence_length size in time is not much bigger than shift
    Note: You can adjust the subsequence length even after you have created the dataset
    """

    def __init__(
        self, sequence_length, subsequence_length, shift, input_size=1, classes_num=2
    ):

        self.sequence = np.random.randint(
            low=1, high=classes_num + 1, size=(sequence_length + shift, input_size)
        )

        self.sequence_length = sequence_length  # Note: this is not exactly the length of self.sequence and that is ok
        self.shift = shift
        self.subsequence_length = subsequence_length

    def __len__(self):
        return self.sequence_length // self.subsequence_length

    def __getitem__(self, index):
        start, stop = (
            index * self.subsequence_length,
            (index + 1) * self.subsequence_length,
        )
        return (
            np.float32(self.sequence[start + self.shift : stop + self.shift, :]),
            np.float32(self.sequence[start:stop, :]),
        )


class ToyShiftedRandomSequenceDatasetOneHot(torch.utils.data.Dataset):
    """
    Basic idea: Each label is sth the network has seen before as an input.
    Note: Proably worse if subsequence_length size in time is not much bigger than shift
    Note: You can adjust the subsequence length even after you have created the dataset
    """

    def __init__(self, sequence_length, subsequence_length, shift, classes_num=2):

        self.sequence_numbers = np.random.randint(
            low=0, high=classes_num, size=(sequence_length + shift,)
        )

        self.sequence = np.zeros(((sequence_length + shift, classes_num)))
        self.sequence[
            np.arange(sequence_length + shift), self.sequence_numbers
        ] = 1  # One-Hot Encoding of the sequence

        self.sequence_length = sequence_length  # Note: this is not exactly the length of self.sequence and that is ok
        self.shift = shift
        self.subsequence_length = subsequence_length

    def __len__(self):
        return self.sequence_length - self.subsequence_length

    def __getitem__(self, index):
        start, stop = index, index + self.subsequence_length
        return (
            np.float32(self.sequence[start + self.shift : stop + self.shift, :]),
            np.long(self.sequence_numbers[stop - 1]),
        )
