# Quite heavily modified, but most code comes from here: https://github.com/BioroboticsLab/IBA
#
# License Text follows:
#
# Copyright (c) Karl Schulz, Leon Sixt
#
# All rights reserved.
#
# This code is licensed under the MIT License.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions :
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import warnings
from functools import reduce
from operator import mul as multiply

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
from skimage.transform import resize
from torch.utils.hooks import RemovableHandle
from tqdm import tqdm


class PerSampleIBA:
    """
    An Object that lets you attribute
    NOTE: This is implemented to be put after 1D, 2D or 3D convolutional layers
    The input shape is assumed to be (batch_size, ) + (?,)*num_dimensions
    where (?,)*num_dimensions represents the feature dimensions
    NOTE: Do not use this directly after a RELU, or you will produce OutOfDistribution Activations
    NOTE: If the layer is called multiple times, we are in the recurrent case.
    Then you have to set the recurrent_flag parameter
    And the input is assumed to have shape (batch_size, recurrent_size) + (?,)*(num_dimensions-1)
    Recurrent size can be the sequence length for sth like an RNN
    """

    def __init__(
        self,
        model,
        layer,
        dataloader_for_estimation,
        device="cpu",
        recurrent_flag=False,
    ):
        self.model, self.layer, self.device, self.recurrent_flag = (
            model,
            layer,
            device,
            recurrent_flag,
        )

        # Feed one batch through the network to check the shape at the layer output
        # and set self.layer_output_shape accordingly.
        def probe(_, __, R):
            # NOTE: self.layer_output_shape does NOT include batch size
            self.layer_output_shape = R.shape[
                1:
            ]  # TODO: Maybe this can be recovered with the state dict?
            return R

        with LayerOutputHook(self.layer, probe), torch.no_grad():
            random_input = torch.randn(
                (1,) + dataloader_for_estimation.dataset[0][0].shape
            ).to(self.device)
            _ = model(random_input)
        assert hasattr(
            self, "layer_output_shape"
        ), "The layer you gave seems to not be part of the networks computation path"

        # Estimate the mean/variance of each feature (assuming a gaussian dist.)
        with LayerOutputHook(
            self.layer, WelfordOnlineEstimator(self.layer_output_shape).to(self.device)
        ) as estimator, torch.no_grad():
            self.model.eval()
            for mini_batch in tqdm(dataloader_for_estimation):
                x = mini_batch[0].to(self.device)
                _ = self.model(x)
            (
                self.mean_R,
                self.std_R,
                self.active_neurons_R,
                n_examples,
            ) = estimator.get_results()

            if (
                n_examples
            ) < 1000:  # TODO: Is this warning accurate for the recurrent case?
                warnings.warn(
                    f"Selected estimator was only fitted on {n_examples} "
                    f"samples. Might not be enough! We recommend 10.000 samples."
                )

    @property
    def active_neurons_percentage(self):
        return self.active_neurons_R.detach().cpu().numpy().astype(int).sum(
            axis=None
        ) / np.prod(self.active_neurons_R.shape)

    def attribute(
        self,
        inputs,
        target,
        batch_size=10,
        optimization_steps=10,
        initial_alpha=5.0,
        beta=10.0,
        sigma=1.0,
        lr=1.0,
        verbose=False,
    ):
        """Input attribution via Information Bottleneck

        Parameters
        ---------
        inputs : torch.Tensor
            Input that you want to attribute to
            Note: shape[0] is assumed to be the batch dimension
            Note: you
        target : int or None
            determines how the model loss part is calculated
            if int, the model loss is a CrossEntropy loss between the output
            of the model and
            if None, the model loss is a KLDivergence loss w.r.t. the
            output of the unmodified model
        batch_size : int
            to stabilize training of the bottleneck, the input is copied
            a number of times equal to batch_size
        optimization_steps : int
            for how many epochs to train the bottleneck
        initial_alpha : float
            initial value for the input/noise tradeoff. Start with
            high value to add very little noise in the beginning
            Note: This value is fed through a sigmoid later to determine
            the tradeoff factor
            Default: 5.0 ; tranlates to: 0.993 * input + 0.007 * noise
        beta : float
            tradeoff factor between model loss and information loss term
        sigma : float or None
            if float, determines the stdev (size) of the gaussian blur that
            is applied to the strength of the noise
            if None, no gaussian blur is applied to the strength of the noise
        lr : float
            Learning rate of the Adam optimizer used to train the bottleneck
        verbose : bool
            Enables additional prints during the execution
        """
        assert inputs.shape[0] == 1, "We can only fit one sample at a time"

        # NOTE: The second dimension is assumed to be the recurrent dimension (e.g. the sequence length for RNNs)
        recurrent_size = inputs.shape[1] if self.recurrent_flag else None

        # Repeat input to stabilize noise training (different noise applied to each example in the batch)
        input_batch = inputs.repeat((batch_size,) + (1,) * len(inputs.shape[1:])).to(
            self.device
        )

        if target is not None:
            CE = torch.nn.CrossEntropyLoss(reduction="mean")
            label_batch = torch.full(
                size=(batch_size,), fill_value=target, device=self.device
            )

            def model_loss_fn(yhat):
                return CE(yhat, label_batch)

        else:
            KLD = torch.nn.KLDivLoss(reduction="batchmean")
            with torch.no_grad():
                label_batch = torch.nn.functional.softmax(
                    self.model(input_batch).detach(), dim=1
                )

            def model_loss_fn(yhat):
                return KLD(torch.nn.functional.log_softmax(yhat, dim=1), label_batch)

        # Insert the bottleneck as a hook into the network
        args = (
            self.layer_output_shape,
            initial_alpha,
            beta,
            sigma,
            self.mean_R,
            self.std_R,
            self.active_neurons_R,
            recurrent_size,
        )
        with LayerOutputHook(
            self.layer, InformationBottleneck(*args).to(self.device)
        ) as bottleneck:

            # Some random prep
            self.model.to(self.device)
            self.model.eval()
            optimizer = torch.optim.Adam(
                params=[bottleneck.alpha], lr=lr
            )  # bottleneck.alpha is basically the noise that we want to optimize

            for i in tqdm(range(optimization_steps)):
                optimizer.zero_grad()

                # Forward pass through the model. NOTE: Also forward pass through the bottleneck hook
                yhat = self.model(input_batch)
                # NOTE: the model_loss_fn takes the WHOLE output of the model
                # but should only compute&return the loss for ONE element of the output
                model_loss = model_loss_fn(yhat)

                # Expectation Value of the KL Divergence Term / the Bottleneck Capacity (read formulas in the paper)
                # NOTE: Taking the mean is equivalent of scaling the sum with 1/K
                information_loss = bottleneck.capacity.float().mean()

                # Optimization step
                if verbose:
                    print(
                        f"Model and Info Loss: {np.round(model_loss.item(), 7)} | {np.round(information_loss.item(), 7)}"
                    )
                    print(
                        f"Predicted classes {torch.argmax(yhat.detach().cpu(), axis=1)}"
                    )
                loss = model_loss + beta * information_loss
                # loss = model_loss
                # loss = information_loss
                loss.backward()
                optimizer.step()

            # Generate saliency map for the output of the layer
            capacity = bottleneck.capacity.detach().cpu().numpy()
            capacity_bits = capacity / float(np.log(2))  # Convert to bits

            return capacity_bits


class LayerOutputHook:
    def __init__(self, layer, object_that_takes_the_output_of_the_layer):
        super().__init__()
        self.layer = layer
        self.object_that_takes_the_output_of_the_layer = (
            object_that_takes_the_output_of_the_layer
        )

    def __enter__(self):
        self.hook_handle = self.layer.register_forward_hook(
            self.object_that_takes_the_output_of_the_layer
        )
        return self.object_that_takes_the_output_of_the_layer

    def __exit__(self, type_, value, traceback):
        self.hook_handle.remove()


class WelfordOnlineEstimator(nn.Module):
    """
    Estimates the feature-wise mean and standard deviation
    Algorithm implementation comes from here:
    ``https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Welford's_online_algorithm``.
    """

    def __init__(self, layer_output_shape, active_neuron_threshold=0.01):
        # NOTE: layer_output_shape does NOT include the batch dimension
        super().__init__()

        self.active_neuron_threshold = active_neuron_threshold
        # self.register_buffer('n_samples', torch.tensor([0], dtype=torch.long)) # do with number?
        self.n_examples = 0

        for buffer_name in ["mean", "M2", "n_nonzero"]:
            self.register_buffer(
                buffer_name, torch.zeros(layer_output_shape, dtype=torch.long)
            )

    def forward(self, _, __, R):

        for example in R:
            self.n_nonzero += (
                example != 0.0
            ).long()  # Not part of the Welford Estimator
            self.n_examples += 1
            delta = example - self.mean
            self.mean = self.mean + delta / self.n_examples
            delta2 = example - self.mean
            self.M2 = self.M2 + delta * delta2

        return R

    def get_results(self):
        assert self.n_examples >= 2
        std = torch.sqrt(self.M2 / (self.n_examples - 1))
        active_neurons = (
            self.n_nonzero.float() / self.n_examples
        ) > self.active_neuron_threshold
        return self.mean, std, active_neurons, self.n_examples


class InformationBottleneck(nn.Module):
    """
    This can restrict the information via the bottleneck approach
    """

    def __init__(
        self,
        layer_output_shape,
        initial_alpha,
        beta,
        sigma,
        mean_R,
        std_R,
        active_neurons_R,
        recurrent_size=None,
    ):
        super().__init__()

        # Enable access to the static parameters in forward
        self.layer_output_shape = (
            layer_output_shape  # NOTE: This does NOT include the batch dimension
        )
        self.mean_R, self.std_R, self.active_neurons_R = mean_R, std_R, active_neurons_R
        self.beta = beta
        self.recurrent_size = recurrent_size

        # Initialize learnable alpha mask, that is going to regulate the information flow

        if not recurrent_size:
            self.alpha = nn.Parameter(
                torch.full(layer_output_shape, initial_alpha), requires_grad=True
            )
        else:
            self.alpha = nn.Parameter(
                torch.full((recurrent_size,) + layer_output_shape, initial_alpha),
                requires_grad=True,
            )

        # Prepare Sigmoid and Gaussian Blur
        self.sigmoid = nn.Sigmoid()
        if sigma is not None:
            kernel_size = (
                int(round(2 * sigma)) * 2 + 1
            )  # Cover 2.5 stds in both directions
            self.blur = MultiDimensionalGaussianKernel(
                kernel_size,
                sigma,
                num_dimensions=len(layer_output_shape[1:]),
                num_channels=layer_output_shape[0],
            )
        else:
            self.blur = None

        if not self.recurrent_size:
            self.register_buffer(
                "capacity", torch.zeros(layer_output_shape, dtype=torch.long)
            )
        else:
            self.register_buffer(
                "capacity",
                torch.zeros((recurrent_size,) + layer_output_shape, dtype=torch.long),
            )
            # We need to keep track of where we are in the buffer. This generator helps . TODO this seems slightly hacky
            self.capacity_buffer_index = loop(0, self.recurrent_size)

    def forward(self, _, __, R):
        # loops between 0 and self.recurrent_size
        i = None if not self.recurrent_size else next(self.capacity_buffer_index)

        # All of the parameters lack a batch dimension (mainly b/c be don't know how big the batches are during __init__)
        # So we need to extend those parameters along the batch dimension to match the shape of R.
        # NOTE: torch's ".repeat" is different form numpy's ".repeat"
        # read here: https://pytorch.org/docs/stable/tensors.html#torch.Tensor.repeat
        repeat_mask = (R.shape[0],) + (1,) * len(self.layer_output_shape)

        # Expand alpha along the batch dimension, so it matches the shape of R
        if not self.recurrent_size:
            α = self.alpha.repeat(repeat_mask)
        else:
            α = self.alpha[i].repeat(repeat_mask)

        # Sigmoid and Blur
        λ = self.sigmoid(α)
        λ = self.blur(λ) if self.blur is not None else λ

        # This is for the calculation of the information loss, it is going to be accessed from outside
        # .mean(dim=0) collapses over the batch dimension, it is only here for stabilization
        # TODO: I think we need to repeat along the batch dimension here as well?
        if not self.recurrent_size:
            self.capacity = (
                kl_divergence(λ, R, mean_R=self.mean_R, std_R=self.std_R)
                * self.active_neurons_R
            ).mean(dim=0)
        else:
            self.capacity[i] = (
                kl_divergence(λ, R, mean_R=self.mean_R, std_R=self.std_R)
                * self.active_neurons_R
            ).mean(dim=0)

        # Expand along the batch dimension BEFORE sampling noise to get different noise for each example.
        # This stabilizes the noise training
        ε = torch.normal(
            mean=self.mean_R.repeat(repeat_mask), std=self.std_R.repeat(repeat_mask)
        )

        Z = λ * R + (1 - λ) * ε

        # TODO do we actually need this active neurons thing?
        Z *= self.active_neurons_R

        return Z


class MultiDimensionalGaussianKernel(nn.Module):
    """Simple Gaussian Blur

    A simple convolutional layer with fixed gaussian kernels, used to smoothen the input
    The input is assumed to have shape (batch_size, num_channels) + (?,)*num_dimensions
    where num_dimensions represents the number of feature dimensions

    Parameters
    ---------
    kernel_size : int
        side length of the gaussian kernel
        (same in each dimension)
    sigma : float
        stdev of the gaussian blur
    num_dimensions : int
        how many feature dimensions exist
    num_channels :
        size of the second dimension of inputs

    Attributes
    ---------
    conv : one of [nn.Conv1d, nn.Conv2d, nn.Conv3d]
        Conv-Layer with weights that corresponds to applying a gaussian blur
        per input channel (no mixing between channels)

    """

    def __init__(self, kernel_size, sigma, num_dimensions, num_channels):
        super().__init__()
        assert (
            kernel_size % 2 == 1
        ), "kernel_size must be an odd number (for padding), {} given".format(
            self.kernel_size
        )

        assert (
            1 <= num_dimensions <= 3
        ), f"Only 1, 2 and 3 dimensional conv layer in pytorch, not {num_dimensions}"

        # Building up an N-Dimensional gaussian kernel step by step
        def gauss_fn(x, var):
            return (1.0 / np.math.sqrt(2.0 * np.pi * var)) * np.exp(-(x ** 2) / 2 * var)

        def gauss_kernel_1dimension_fn(kernel_size, sigma):
            return gauss_fn(
                np.arange(-(kernel_size // 2), 1 + (kernel_size // 2)), sigma ** 2
            )

        def gauss_kernel_Ndimensions_fn(kernel_size, sigma, N):
            return np.multiply(
                *np.meshgrid(*([gauss_kernel_1dimension_fn(kernel_size, sigma)] * N))
            )

        # Dimension fiddling and stuff
        if num_dimensions == 1:
            gauss_kernel = gauss_kernel_1dimension_fn(kernel_size, sigma)
        else:
            gauss_kernel = gauss_kernel_Ndimensions_fn(
                kernel_size, sigma, N=num_dimensions
            )
        gauss_kernel = gauss_kernel / np.sum(
            gauss_kernel
        )  # Normalize not soo necessary (sum should already be ~1), but why not
        gauss_kernel = torch.Tensor(gauss_kernel)
        # Expand for use with nn.ConvNd functions.
        gauss_kernel_weights = gauss_kernel.repeat(
            (num_channels, 1) + (1,) * num_dimensions
        )

        # Combining the gaussian kernel with a fitting nn.ConvNd layer
        fitting_pytorch_convNd_layer = [nn.Conv1d, nn.Conv2d, nn.Conv3d][
            num_dimensions - 1
        ]

        self.conv = fitting_pytorch_convNd_layer(
            in_channels=num_channels,
            out_channels=num_channels,
            kernel_size=kernel_size,
            stride=1,
            padding=kernel_size // 2,
            groups=num_channels,
            bias=False,
        )
        self.conv.weight.data.copy_(gauss_kernel_weights)
        self.conv.weight.requires_grad = False

    def forward(self, x):
        return self.conv(x)


def kl_divergence(λ, R, mean_R, std_R):
    """Computes the KL Divergence between the noise (Q(Z)) and
    the noised activations P(Z|R))."""
    # We want to compute: KL(P(Z|R) || Q(Z))
    # As both distributions are normal distribution, we need the
    # mean and variance of both. We can simplify the computation
    # by normalizing R to R' = (R - E[R]) / std(R) [1] and ε' ~ N(0,1).
    # The KL divergence is invariant under scaling.
    #
    # For the mean and var of Z|R, we have:
    # Z' = λ * R' + (1 - λ) * ε'
    # E[Z'|R'] = λ E[R']                    [2]
    # Var[Z'|R'] = (1 - λ)**2               [3]
    # Remember that λ(R) and therefore the λ becomes a constant if
    # R is given.
    #

    # Normalizing [1]
    norm_R = (R - mean_R) / std_R

    # Computing mean and var Z'|R' [2,3]
    var_Z = (1 - λ) ** 2
    mu_Z = norm_R * λ

    log_var_Z = torch.log(var_Z)

    # For computing the KL-divergence:
    # See eq. 7: https://arxiv.org/pdf/1606.05908.pdf
    capacity = -0.5 * (1 + log_var_Z - mu_Z ** 2 - var_Z)
    return capacity


def loop(start, stop):
    current = start
    while True:
        current += 1
        if current >= stop:
            current = start
        yield current


def rescale(capacity_map, desired_shape):
    # Scale bits to the pixels
    def product(factors):
        return reduce(multiply, factors)

    saliency_map *= product(capacity_map.shape) / product(desired_shape)
    # Upscale to model input shape
    return resize(capacity_map, desired_shape, order=1, preserve_range=True)
