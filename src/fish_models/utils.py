import numpy as np


def turn_angle(previous_direction, desired_direction):
    """Calulate 2d turn angle

    Calculate the shortest 2d turn angle given an old and a new direction
    vector in 2d space

    Parameters
    ---------
    previous_direction : np.ndarray of shape (2,) or (2,n)
        Direction you are currently headed, does not need to be normalized
        Either single direction vector or two x/y lists for n dir vectors
    desired_direction : np.ndarray of shape (2,) or (2,n)
        Direction you want to go to, does not need to be normalized
        Either single direction vector or two x/y lists for n dir vectors
    Returns
    -------
    turn_angle : float or np.ndarray of shape (n,)
        Turn angle in radians. Can have values from -pi to +pi
    """

    assert previous_direction.shape == desired_direction.shape
    assert previous_direction.shape[0] == 2

    # Avoid zero vector problems
    if np.all(np.isclose(desired_direction, 0)):
        if len(previous_direction.shape) == 2:
            turn_angle = np.zeros(previous_direction.shape[1])
        else:
            turn_angle = 0.0
    else:
        # Calculate turn angle in rad
        desired_abs_angle = np.arctan2(desired_direction[1], desired_direction[0])
        previous_abs_angle = np.arctan2(previous_direction[1], previous_direction[0])
        turn_angle = desired_abs_angle - previous_abs_angle

        turn_angle += np.pi
        turn_angle = turn_angle % (2 * np.pi)
        turn_angle -= np.pi

    return turn_angle


def turn_angular_velocity(
    previous_direction,
    desired_direction,
    time_step_s,
    noise=None,
    max_angular_velocity_rad_s=None,
):
    """Calculates angular velocity in rad/s

    Calculate the shortest 2d turn angle given an old and a new direction
    vector in 2d space, then calculate the angular velocity in rad/s required
    to perform that turn in one time step

    Parameters
    ---------
    previous_direction : [float, float]
        Direction you are currently headed, does not need to be normalized
    desired_direction : [float, float]
        Direction you want to go to, does not need to be normalized
    time_step_s : float
        The length on one time step in seconds
    noise : float or callable, optional
        If float, gaussian noise with stdev of noise is added to turn ANGLE
        If not a float and not None, noise is assumed to be a random-number
        generating callable. Noise from that callable is added to turn ANGLE
    max_angular_velocity_rad_s : float, optional
        If provided, the absolute turn speed value cannot exceed this value
    Returns
    -------
    turn_angular_velocity : float
        Angular velocity required for the turn in radians/s
        Note: Angular velocity to the left is positive, to the right negative
    """
    turn_angle_ = turn_angle(previous_direction, desired_direction)

    # Stochasticity
    if isinstance(noise, float):
        turn_angle_ = turn_angle_ + np.random.default_rng().normal(loc=0, scale=noise)
    elif noise is not None:
        turn_angle_ = turn_angle_ + noise()

    # Turn speed required to perform turn_angle in one time step
    turn_angular_velocity = turn_angle_ / time_step_s

    # Clip to max turn
    if max_angular_velocity_rad_s is not None:
        turn_angular_velocity = np.clip(
            turn_angular_velocity,
            -max_angular_velocity_rad_s,
            max_angular_velocity_rad_s,
        )

    return turn_angular_velocity
