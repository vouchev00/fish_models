import pytest
import robofish.io
import fish_models

import numpy as np
import pandas as pd
from pathlib import Path
from tqdm import tqdm

import matplotlib.pyplot as plt

np.set_printoptions(suppress=True, precision=3)


folder_path = robofish.io.utils.full_path(__file__, "../../../resources/test_folder/")
file_path = folder_path / "test_f.hdf5"
if file_path.exists():
    file_path.unlink()


@pytest.fixture(scope="session")
def file_setup_fixture():
    return file_setup()


def file_setup():
    # generate io file
    f = robofish.io.File(file_path, mode="x", world_size_cm=[100, 100], frequency_hz=25)
    num = 10
    positions = np.empty((10, 2))
    positions[:, 0] = np.cos(np.linspace(0, 2 * np.pi, num=num))
    positions[:, 1] = np.sin(np.linspace(0, 2 * np.pi, num=num))
    f.create_entity("fish", positions=positions)

    bins = 3
    raycast_options = {
        "n_fish_bins": bins,
        "n_wall_raycasts": bins,
        "fov_angle_fish_bins": np.pi,
        "fov_angle_wall_raycasts": np.pi,
        "world_bounds": ([-50, -50], [50, 50]),
        # "far_plane": 142,
    }

    # Raycast in cm
    raycast = fish_models.gym_interface.Raycast(**raycast_options)

    # IO Files in cm, actions in m/s

    # data_folder = Path(
    #     "/home/andi/mnt/trackdb_offline/trainingdata/live_female_female/train"
    # )
    dset = fish_models.datasets.io_dataset.IoDataset(
        folder_path,
        raycast,
        output_strings=["views", "actions"],
        reduce_dim=2,
        max_files=1,
    )

    # TODO: CHECK LAST (Later comment: what does this comment mean?)
    columns = [f"fish {i}" for i in range(bins)] + [f"walls {i}" for i in range(bins)]
    print(pd.DataFrame(dset.poses[0, 0], columns=["x", "y", "ori"]))
    print(
        "First 10 of the sequence\n",
        pd.DataFrame(dset[:10]["views"], columns=columns),
        pd.DataFrame(dset[:10]["actions"], columns=["speed", "turn"]),
    )

    model = fish_models.models.andi.lookup_model.LookupModel(tau=0.01)
    model.train(dset)

    # model.train(dset.sequence[len(dset) // 2 : len(dset) // 2 + 10])

    # def teardown():
    #     pass

    # request.addfinalizer(teardown)

    return raycast, dset, model


def test_model_actions(file_setup_fixture):
    raycast, dset, model = file_setup_fixture
    # import matplotlib.pyplot as plt

    # PLOT THE MIN, MAX, MEDIAN of the dataset

    # plt.plot(np.median(dset.sequence, axis=0), label="median")
    # plt.plot(np.min(dset.sequence, axis=0), label="min")
    # plt.plot(np.max(dset.sequence, axis=0), label="max")
    # plt.legend()
    # plt.show()

    # plt.h ist(dset.sequence[:, 50])

    # plt.scatter(
    #     dset.sequence[:, 50], dset.sequence[:, 51], alpha=0.005, edgecolors="none"
    #

    # PLOT THE 2D HISTOGRAM OF THE SPEED AND TURN VALUES

    # plt.hist2d(dset.sequence[:, 50], dset.sequence[:, 51], bins=[250, 251])
    # plt.title("Turn Speed distribution of live_female_female train dataset")
    # plt.xlabel("speed cm/s")
    # plt.ylabel("turn rad/s")
    # plt.colorbar()
    # plt.show()

    for idx, row in enumerate(dset):
        view = row["views"]
        action = row["actions"]
        suggested_action = model.choose_action(view)
        # Since this is a lookup model, it should suggest the exact same action
        # A duplicate of views in the data is unprobable and would cause an error here
        # print(suggested_action)
        assert np.isclose(
            suggested_action, action
        ).all(), f"suggested actions were not identical to dataset action in index {idx}.\nView:\t{view}\nExpected action:\t{action}\nSuggested action:\t{suggested_action}."


def test_lookup_model(file_setup_fixture, plot=False):
    raycast, dset, model = file_setup_fixture

    trackset_len = 9

    generator = fish_models.gym_interface.TrackGeneratorGymRaycast(
        model, dset.world_size, dset.frequency, raycast=raycast
    )

    for i in tqdm(range(1)):

        initial_poses = np.array([dset.poses[0, 0, 0]]) if i == 0 else None
        track = generator.create_track(1, trackset_len, initial_poses, histories=["view", "action"])

        # print("turn_speed")
        # print(
        #     "turn_speed\n",
        #     pd.DataFrame(dset.speeds_turns[0, :9], columns=["speed", "turn"]),
        # )
        # print(
        #     "actions\n",
        #     pd.DataFrame(generator.action_history[0], columns=["speed", "turn"]),
        # )

        # # print(generator.view_history)
        generator_combined = np.concatenate(
            [
                track[:, : trackset_len - 1],
                generator.view_history,
                generator.action_history,
            ],
            axis=2,
        )

        columns = (
            ["pos_x", "pos_y", "ori"]
            + [f"fish {i}" for i in range(raycast.n_fish_bins)]
            + [f"walls {i}" for i in range(raycast.n_wall_raycasts)]
            + ["speed", "turn"]
        )
        print(
            "Generated sequence\n",
            pd.DataFrame(
                generator_combined[0],
                columns=columns,
            ),
        )

        # state_hist_generator = generator.state_history

        # state_hist_generator[:, :, :2] *= 100

        # dset_combined = np.concatenate(
        #     [dset.poses[0, : trackset_len - 1], dset.sequence[: trackset_len - 1]],
        #     axis=1,
        # )

        # columns = [
        #     "pos_x",
        #     "pos_y",
        #     "ori",
        #     "vfish_0",
        #     "vfish_1",
        #     "vwall_0",
        #     "vwall_1",
        #     "vwall_2",
        #     "speed",
        #     "turn",
        # ]
        # print("generator")
        # print(pd.DataFrame(generator_combined[0], columns=columns))
        # # print(pd.DataFrame(generator_combined[1], columns=columns))

        # print("generator_state_hist is equal to track poses (generated)")

        # # print(pd.DataFrame(state_hist_generator[0], columns=["x", "y", "ori"]))
        # print("dset")
        # print(pd.DataFrame(dset_combined, columns=columns))
        # print("diff -> fish0")
        # print(pd.DataFrame(generator_combined[0] - dset_combined, columns=columns))

        # # combined = np.concatenate(
        # #     [dset.speeds_turns[:, : trackset_len - 1], generator.action_history], axis=2
        # # )
        # # print(combined)

        # ioF = generator.as_io_file(track).save_as("generated.hdf5")

        # plt.figure(figsize=(7, 7))
        if plot:
            colors = ["red", "blue"]
            for fish_id in range(dset.poses.shape[1]):
                # print(dset.poses[fish_id, : len(track[0])])
                plt.plot(
                    dset.poses[0, fish_id, : len(track[0]), 0],
                    dset.poses[0, fish_id, : len(track[0]), 1],
                    c=colors[fish_id],
                    alpha=0.5,
                    lw=5,
                    label="original",
                )
                plt.plot(
                    track[fish_id, :, 0],
                    track[fish_id, :, 1],
                    alpha=1,
                    label="reconstructed",
                )

    if plot:
        plt.legend()
        plt.show()


def test_cleanup():
    if file_path.exists():
        file_path.unlink()


if __name__ == "__main__":
    setup = file_setup()
    test_model_actions(setup)
    test_lookup_model(setup, plot=True)
