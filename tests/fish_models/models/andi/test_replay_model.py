import pytest
import robofish.io
import robofish.io.utils as utils
import fish_models

import numpy as np
from pathlib import Path

import matplotlib.pyplot as plt


plotting = False


@pytest.fixture(scope="session")
def file_setup_fixture():
    return file_setup()


def file_setup():
    raycast_options = {
        "n_fish_bins": 5,
        "n_wall_raycasts": 5,
        "fov_angle_fish_bins": np.pi,
        "fov_angle_wall_raycasts": np.pi,
        "world_bounds": ([-50, -50], [50, 50]),
        # "far_plane": 142,
    }

    # Raycast in cm
    raycast = fish_models.gym_interface.Raycast(**raycast_options)

    # IO Files in cm, actions in m/s
    data_folder = Path(
        "/home/andi/mnt/trackdb_offline/trainingdata/live_female_female/train"
    )
    f = robofish.io.File(list(data_folder.glob("*.hdf5"))[0])

    turns_speeds = f.entity_speeds_turns * f.frequency

    models = []
    for fish_id in range(2):
        m = fish_models.models.andi.replay_model.ReplayModel()
        m.train(turns_speeds[fish_id])
        models.append(m)

    return raycast, f, models


def test_replay_model(file_setup_fixture):
    raycast, f, models = file_setup_fixture

    generator = fish_models.gym_interface.TrackGeneratorGymRaycast(
        models, f.world_size, f.frequency, raycast=raycast
    )

    poses = f.entity_poses_calc_ori_rad
    initial_poses = poses[:, 0]

    print("First Poses:\n", poses[:, :2])

    test_timesteps = 800

    track = generator.create_track(
        n_guppies=2, trackset_len=test_timesteps, initial_poses=initial_poses
    )

    print("First track poses\n", track[:, :2])
    print("Action history\n", generator.action_history[:, :2] / f.frequency)

    # TODO: TEMPORAL LAG IN GUPPY GYM! MATHIS or ME?!
    # track[:, :-1, 2] = track[:, 1:, 2]
    # track = track[:, :-1]  # Shorten track
    # test_timesteps -= 1  # Shorten track

    ioF = generator.as_io_file(track).save_as("__generated.hdf5", strict_validate=False)

    delta = poses[:, :test_timesteps] - track
    delta = utils.limit_angle_range(delta, _range=(-np.pi, np.pi))

    if plotting:
        plt.figure(figsize=(7, 7))
        plt.title("Original Track vs. reconstructed track, high alpha means high delta")
        colors = ["red", "blue"]
        for fish_id in range(2):
            plt.plot(
                poses[fish_id, : len(track[0]), 0],
                poses[fish_id, : len(track[0]), 1],
                c=colors[fish_id],
                alpha=0.5,
                label=f"Original Track {fish_id}",
            )

            rgba_colors = np.zeros((test_timesteps, 4))
            # Hacky way of creating red for the first fish and blue for the second fish
            rgba_colors[:, fish_id * 2] = 1.0

            # the fourth column needs to be your alphas
            rgba_colors[:, 3] = abs(np.mean(delta[fish_id], axis=1))
            rgba_colors[:, 3] -= min(rgba_colors[:, 3])
            rgba_colors[:, 3] /= max(rgba_colors[:, 3])

            plt.scatter(
                track[fish_id, :, 0],
                track[fish_id, :, 1],
                c=rgba_colors,
                label=f"Reconstructed track {fish_id}",
            )
        plt.legend()

        plt.figure()
        plt.title("ori over time")
        for fish_id in range(2):
            plt.plot(track[fish_id, :, 2], label=f"reconstructed orientation {fish_id}")
            plt.plot(
                poses[fish_id, : len(track[0]), 2],
                label=f"original orientation {fish_id}",
            )
        plt.legend()

        plt.figure()
        plt.title("Delta over time")
        for fish_id in range(2):
            for i, meaning in enumerate(["x", "y", "ori"]):
                plt.plot(delta[fish_id, :, i], label=f"Delta {fish_id}, {meaning}")

        plt.legend()
        plt.show()

    print(
        f"Maximal difference between original poses and track is {np.max(abs(delta))}"
    )
    # TODO: lower tolerance from extremely high number
    assert (abs(delta) < 2.5).all()


if __name__ == "__main__":
    plotting = True
    setup = file_setup()
    test_replay_model(setup)
