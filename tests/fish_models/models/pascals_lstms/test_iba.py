import pytest
import torch

from fish_models.models.pascals_lstms.iba import MultiDimensionalGaussianKernel


def test_multidimensional_gaussian_kernel():
    kernel = MultiDimensionalGaussianKernel(
        kernel_size=5, sigma=1, num_dimensions=2, num_channels=3
    )
    image = torch.randn((1, 3, 224, 224))

    blurred_image = kernel(image)

    assert blurred_image.max() <= image.max()
