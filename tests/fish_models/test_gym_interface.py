import functools
from pathlib import Path
from typing import Callable, Tuple

import numpy as np
import pytest
import robofish.gym_guppy
import robofish.io.utils as utils

import fish_models

test_file = utils.full_path(__file__, "../resources/test.hdf5")
if test_file.exists():
    test_file.unlink()


def test_raycast_generation():
    raycast_options = {
        "n_fish_bins": 20,
        "n_wall_raycasts": 20,
        "fov_angle_fish_bins": np.pi,
        "fov_angle_wall_raycasts": np.pi,
        "world_bounds": ([-0.5, -0.5], [0.5, 0.5]),
        "far_plane": np.sqrt(2),
    }
    raycast = fish_models.Raycast(**raycast_options)

    assert raycast.options == raycast_options


def test_raycast_class(model=None):

    raycast = fish_models.Raycast(
        n_fish_bins=20,
        n_wall_raycasts=20,
        fov_angle_fish_bins=np.pi,
        fov_angle_wall_raycasts=np.pi,
        world_bounds=([-0.5, -0.5], [0.5, 0.5]),
    )

    class TestEnv(robofish.gym_guppy.envs.GuppyEnv):
        def _reset(self):
            for _ in range(1):
                guppy = fish_models.gym_interface.RaycastBasedModelGuppy(
                    model=model,
                    raycast=raycast,
                    world_conversion_factor=1,
                    frequency=25,
                    guppy_env=self,
                )
                self._add_guppy(guppy)

    return TestEnv(time_step_s=1/25)


def test_model():
    class ExampleModel(fish_models.gym_interface.AbstractRaycastBasedModel):
        def __init__(self):
            pass

        def choose_action(self, view: np.ndarray) -> Tuple[float, float]:

            return (0.0, 0.0)

    model = ExampleModel()
    env = test_raycast_class(model)
    return model


def test_generate_robofishIO_trackset():

    n_guppies = 2
    trackset_len = 50

    raycast = fish_models.Raycast(
        n_fish_bins=20,
        n_wall_raycasts=20,
        fov_angle_fish_bins=np.pi,
        fov_angle_wall_raycasts=np.pi,
        world_bounds=([-0.5, -0.5], [0.5, 0.5]),
    )
    generator = fish_models.gym_interface.TrackGeneratorGymRaycast(
        test_model(), [100, 100], 25, raycast=raycast,
    )

    track = generator.create_track(n_guppies=n_guppies, trackset_len=trackset_len)

    assert track is not None
    assert track.shape == (n_guppies, trackset_len, 3)

    generator.as_io_file(track, test_file, experiment_setup="Test")

    assert test_file.exists()
    test_file.unlink()


def test_raycast():

    raycast = fish_models.Raycast(
        n_fish_bins=20,
        n_wall_raycasts=20,
        fov_angle_fish_bins=np.pi,
        fov_angle_wall_raycasts=np.pi,
        world_bounds=([-0.5, -0.5], [0.5, 0.5]),
        far_plane=np.sqrt(2),
    )

    # No other fishes
    state = np.array([[0.0, 0.0, 0.0]])
    view = raycast.cast(state, 0)
    assert view is not None

    assert view.shape == (40,)
