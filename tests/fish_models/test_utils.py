import numpy as np
import pytest

import fish_models.utils


def test_turn_angle():
    is_zero = fish_models.utils.turn_angle(np.zeros(2), np.zeros(2))
    assert is_zero == 0.0

    right, up, left, down = np.array([[1, 0], [0, 1], [-1, 0], [0, -1]])

    # Turn right
    assert fish_models.utils.turn_angle(right, up) == np.pi / 2
    assert fish_models.utils.turn_angle(right, up + left) == np.pi * 3 / 4

    # Turn left
    assert fish_models.utils.turn_angle(right, down) == -np.pi / 2
    assert fish_models.utils.turn_angle(right, down + left) == -np.pi * 3 / 4

    # Scaling should not matter
    assert fish_models.utils.turn_angle(up, left * 100000) == np.pi / 2
    assert fish_models.utils.turn_angle(down, left * 0.00001) == -np.pi / 2

    # Multi-Input
    four_right = np.array([[1, 1, 1, 1], [0, 0, 0, 0]])
    up_upleft_down_downleft = np.array([[0, -1, 0, -1], [1, 1, -1, -1]])
    assert all(
        fish_models.utils.turn_angle(four_right, up_upleft_down_downleft)
        == np.array([np.pi / 2, np.pi * 3 / 4, -np.pi / 2, -np.pi * 3 / 4])
    )


def test_turn_angular_velocity():
    right, up = np.array([[1, 0], [0, 1]])

    # If one time step = 1 second
    assert fish_models.utils.turn_angular_velocity(
        right, up, 1
    ) == fish_models.utils.turn_angle(right, up)

    assert fish_models.utils.turn_angular_velocity(right, up, 1 / 25) == 25 * np.pi / 2
    assert fish_models.utils.turn_angular_velocity(up, right, 1 / 25) == -25 * np.pi / 2

    # Clipping
    assert (
        fish_models.utils.turn_angular_velocity(
            right, up, 1, max_angular_velocity_rad_s=np.pi / 4
        )
        == np.pi / 4
    )
    assert (
        fish_models.utils.turn_angular_velocity(
            up, right, 1, max_angular_velocity_rad_s=0.001
        )
        == -0.001
    )

    # Clipping + Randomness Gauss
    for _ in range(10):
        random = fish_models.utils.turn_angular_velocity(
            right, up, 1, noise=10000.0, max_angular_velocity_rad_s=np.pi / 4
        )
        assert -np.pi / 4 <= random <= np.pi / 4

    # Clipping + Randomness Closure
    for random_number in [0, -50, -6914241, 495199, 0.0041, -0.4, 91, 51, 42, -84]:
        random = fish_models.utils.turn_angular_velocity(
            right,
            up,
            1,
            noise=lambda: random_number,
            max_angular_velocity_rad_s=np.pi / 4,
        )
        assert -np.pi / 4 <= random <= np.pi / 4
